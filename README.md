# Wishlist Server

Server for Wishlist application.

Current running server, including documentation, may be found at [https://server.wishlist.krzysztof.tatarynowicz.eu](https://server.wishlist.krzysztof.tatarynowicz.eu).

## Overview

The Wishlist Server provides RESTful API for managing your wishes and fulfill those of other users. Any user can create
and maintain their own list of wishes, where they specify what they would like to get as a gift. You may consider
Wishlist as another todo app but with several enhancements.

### Base features

The Wishlist Server provides the following features:

- making, describing and managing your wishes, 
- browsing and filtering other users wishes,
- fulfilling the wishes of others (as well as having your wishes fulfilled!),
- being up to date with your relatives' and friends' preferences.

You can see the full documentation for developers after installing the project. The documentation will be then available
under `/docs`.

## Installation

### Prerequisites

The project runs on [PHP 8.0](https://www.php.net/) and uses [Laravel 8.12](https://laravel.com/docs/8.x), therefore you
will require to install these (if you haven't already). All dependencies are managed using [Composer 2.0.7](https://getcomposer.org/).

### Installation

To install the project you simply need to clone the repository and run `composer install`. If you're using a local PHP
development server manager such as [XAMPP](https://www.apachefriends.org/pl/index.html), [WAMP](https://www.wampserver.com/en/)
or any other, you just need to put the project files inside the proper manager directory (for example`/htdocs` for XAMPP
or `/www` for WAMP). Otherwise, you need to run `php artisan serve` command inside the project directory to run the
server.

## Testing

The Wishlist Server is broadly tested using PHPUnit. To run all tests simply run the `php artisan test` command. There
are some parts that (in my opinion) did not require covering with tests, such as pagination and controlling it with
`per_page` and `page` params. This is part of the Laravel framework which I believe works just fine without me testing
it in this project.
