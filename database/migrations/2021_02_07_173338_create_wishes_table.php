<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("wishes", function (Blueprint $table) {
            $table->id();
            $table->string("name", 255);
            $table->text("description")->nullable()->default(null);
            $table->date("due_date")->nullable();
            $table->unsignedBigInteger("owner_id");
            $table->unsignedBigInteger("executor_id")->nullable()->default(null);
            $table->string("status")->nullable(false)->default("open");
            $table->timestamps();

            $table->foreign("owner_id")
                ->references("id")->on("users")
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign("executor_id")
                ->references("id")->on("users")
                ->cascadeOnUpdate()
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("wishes", function (Blueprint $table) {
            $table->dropForeign(["owner_id", "executor_id"]);
        });
        Schema::dropIfExists("wishes");
    }
}
