<?php
declare(strict_types=1);
namespace Database\Factories;

use App\Models\User;
use App\Models\Wish;
use Illuminate\Database\Eloquent\Factories\Factory;

class WishFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Wish::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            "name" => $this->faker->words(3, true),
            "description" => $this->faker->text(),
            "due_date" => $this->dueDateFunction(),
            "owner_id" => User::factory(),
            "executor_id" => null,
            "status" => $this->statusFunction(),
        ];
    }

    /**
     * Returns the function generating "due date" with probability.
     *
     * @return callable
     */
    private function dueDateFunction(): callable
    {
        return function () {
            return ((mt_rand(0, 99) / 100) >= 0.9)
                ? $this
                    ->faker
                    ->dateTimeBetween("now", "+1 year")
                    ->format("Y-m-d")
                : null;
        };
    }

    /**
     * Returns the function generating "status" with probability.
     *
     * @return callable
     */
    private function statusFunction(): callable
    {
        return function () {
            $random = mt_rand(0, 99) / 100;

            if ($random < 0.33) {
                return "open";
            } else if ($random < 0.66) {
                return "in_progress";
            } else {
                return "fulfilled";
            }
        };
    }
}
