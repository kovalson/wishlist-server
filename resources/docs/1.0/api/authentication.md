# Authentication

- [Login](#login)
- [Logout](#logout)

<a name="login"></a>
## Login

Login is the starting point of using the Wishlist Server. Beside the landing page, documentation and other auxiliary
information, the application requires users to be authorized to perform any actions.

<br>

Wishlist Server requires credentials to authenticate a user. These credentials are `email` and `password`. The API
accepts only the `POST` method to log users in. After a successful authentication the server returns `user` data along
with an `access_token` that must be used in further requests.

<br>

As for now, <ins>there is no option to register in Wishlist</ins>. All users must be added to the database "by hand" or
generated in other way.

### Endpoint

| Method | URI |
|:-------|:----|
| POST   | `/api/login` |

### Data parameters

The following data parameters are required:

```json
{
    "email": "email",
    "password": "string"
}
```

### Response

A successful login response has the structure presented below.

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "user": "User",
        "access_token": "string"
    }
}
```

The `user` attribute contains a [User resource](/{{route}}/{{version}}/resources/user).

> {info.fa-info-circle} Authenticated user can be authenticated again. The request response will be exactly the same as
> if guest was authenticated, though such action will result in refreshing the `access_token`. This new token will be a
> valid one and therefore should be saved.

### Examples

<br>

#### Successful login request

```http
POST /api/login?email=admin@wishlist&password=123
```

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "user": {
            "id": 1,
            "name": "Administrator",
            "email": "admin@wishlist",
            "email_verified_at": null,
            "created_at": "2021-01-23T16:46:28.000000Z",
            "updated_at": "2021-01-23T16:46:28.000000Z"
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNDhlYmU5YzUyOGY3NDE5YzdhNjAwZmM4MzA2MGQxOWY4YzdhMTllZWNmMTc2OGQ5NjQ1MTU4NzhlNTJjYzg3NGRhNzllNmM2YjM0ZDU5ODgiLCJpYXQiOiIxNjExNjgzODA1Ljk1MTY2MyIsIm5iZiI6IjE2MTE2ODM4MDUuOTUxNjY4IiwiZXhwIjoiMTY0MzIxOTgwNS42NjQ4MTMiLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.KERvrMoPrdyT7Ec09lDbkzw6f7AzKmPAcmNdo6rS9_vOrVFes8n6MYgXIpZKMLEwbLwulxJvetEDq8uzBmV9HwUrJDxjn7qjHTGVAOG3ItdODdh2f0Ls4SWC_6c8VG4Ok0SYKvUpOuQCeOQSadzL6icSf-svyvWz8VxPqaWiAOP6lcyipug4GE2HqsM7FFGmyo92801aQnolPu8KiwXE3DthNYejS64_75UFMmF2TgO_iQdVechxsI00N4zfXH5lUNRd0H7-3lNXezamilp2vc_pjOt2SF8J2krKVrkQ8SBpWXeCpdTwuUlkyZ-axHtlbj10Tm1GXviEVdO0XOoPy10uB_Et4Tz-JZ07Tr12FJVA8FqoItuCbEAi4-hwh9BZJZ1afrPlfER1swXcCjRgBAbOTpCkMXLxtwILAmNZ9-tWhSyCPwDA4mmSY1_S0m4h5rp-Zno-vBZc4K0MUdtqL3T8lZy1mXbiv3Lie0dJh7PAP_uIF9Ma5sxSf6Yf-eydAyjgyQOQrkDHE4ppbDXyQcEFVJIZKbV7891QJLACuD5BKU2zMGh_BA2BQ-HCuodQTySb5bKhffZaX1ZFMriUi9wMd8t_KPe8nQrGW8zSG6SSQ4q8bPa6XFY6VIoiyhLvUCVgFDghSUIHtQ30aRc5KNnLpCHOHY-z1Sl8As-o1hU"
    }
}
```

<br>

#### Non-existing user login request

```http
POST /api/login?email=non@existing&password=123456
```

```json
{
    "status": 401,
    "success": false,
    "message": "Authentication failed - user was not found.",
    "data": null
}
```

<a name="logout"></a>
## Logout

### Endpoint

| Method | URI |
|:-------|:----|
| POST   | `/api/logout` |

### Data parameters

No parameters are required for this request.

### Response

A successful response has the following structure:

```json
{
    "status": 200,
    "success": true,
    "message": "The user has been successfully logged out.",
    "data": null
}
```
