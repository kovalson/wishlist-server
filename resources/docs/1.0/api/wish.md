# Wish

- [Create](#create)
- [Browse](#browse)
- [Read](#read)
- [Update](#update)
- [Take](#take)
- [Delete](#delete)

<a name="create"></a>
## Create

Authorized users may create their wishes. These wishes will be then visible for other users. See the [Wish resource](/{{route}}/{{version}}/resources/wish)
page for more details.

### Endpoint

Use the following route to create a wish:

```http
POST /api/wishes
```

### Data parameters

```json
{
    "name": "string",
    "description?": "string|null",
    "due_date?": "string|null",
    "allowed_users?": "number[]|null"
}
```

<br>

| Attribute | Default value | Rules | Description |
|:----------|:--------------|:------------|:------|
| `name` | N/A | max. 255 chars | The name that defines the wish
| `description` | `null` | | Optional description that specifies the details
| `due_date` | `null` | format: `YYYY-MM-DD`, after today | Optional due date of fulfilling the wish
| `allowed_users` | `null` | | Optional array of ids of users that are allowed to fulfill the wish. When `null` or not present, all users will be allowed

### Response

A successful response after creating a wish is as follows:

```json
{
    "status": 201,
    "success": true,
    "message": null,
    "data": {
        "wish": "Wish"
    }
}
```

The `wish` data attribute contains a [Wish resource](/{{route}}/{{version}}/resources/wish).

<a name="browse"></a>
## Browse

Authorized users may browse the list of all wishes. By default, only the `open` ones are displayed, but the view can be
adjusted using filter parameters. Note that the wishes are always paginated as there might be many of them, therefore
there will be also a [Pagination resource](/{route}}/{{version}}/resources/pagination) included in the response.

### Endpoint

Use the following route to browse wishes:

```http
GET /api/wishes
```

### Query parameters

```json
{
    "page?": "number",
    "per_page?": "number",
    "filter?": "fieldset",
    "sort?": "string"
}
```

<br>

| Parameter | Default value | Rules | Description |
|:----------|:--------------|:------------|:------|
| `page` | `1` | `integer`, `min:1` | Current page number
| `per_page` | `15` | `integer`, `min:1` | Number of results per page
| `filter` | N/A | `fieldset` | Wishes filters
| `sort` | N/A | `sort` | Columns to sort by

### Response

A successful listing response is as follows:

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "wishes": {
            "data": "Wish[]",
            "links": {
                "first": "string",
                "last": "string",
                "prev": "string|null",
                "next": "string|null"
            },
            "meta": {
                "current_page": "number",
                "from": "number|null",
                "last_page": "number",
                "path": "string",
                "per_page": "number",
                "to": "number|null",
                "total": "number"
            }
        }
    }
}
```

> {info.fa-info-circle} Note that the attribute is now named `"wishes"` (in plural) since it returns multiple resources.

<a name="read"></a>
## Read

Authorized users may read wishes that they are allowed to.

### Endpoint

Use the following route to read a wish:

```http
GET /api/wishes/{id}
```

### Query parameters

There are no query parameters available for this route.

### Response

A successful response is as follows:

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "wish": "Wish"
    }
}
```

If you're not among the allowed users, the following response will be returned:

```json
{
    "status": 403,
    "success": false,
    "message": "You are not allowed to see the requested wish.",
    "data": null
}
```

If the requested wish doesn't resource, the following response will be returned:

```json
{
    "status": 404,
    "success": false,
    "message": "The requested wish does not exist.",
    "data": null
}
```

<a name="update"></a>
## Update

Authorized users may update their own wishes after creating them.

### Endpoint

Use the following route to update a wish:

```http
PATCH /api/wishes/{id}
```

> {info.fa-info-circle} Note that this route is available for wish owner only.

### Data parameters

```json
{
    "name?": "string",
    "description?": "string|null",
    "due_date?": "string|null",
    "allowed_users?": "number[]|null",
    "status?": "string|null"
}
```

<br>

| Attribute | Default value | Rules | Description |
|:----------|:--------------|:------------|:------|
| `name` | N/A | max. 255 chars | Optional name that defines the wish
| `description` | `null` | | Optional description that specifies the details
| `due_date` | `null` | format: `YYYY-MM-DD`, after today | Optional due date of fulfilling the wish
| `allowed_users` | `null` | | Optional array of ids of users that are allowed to fulfill the wish. When `null` or not present, all users will be allowed
| `string` | `null` | One of the [wish statuses](/{{route}}/{{version}}/resources/wish#statuses) | Optional new status of the wish

> {info.fa-info-circle} Note that when the wish is in progress, the executor will still have access to that wish even
> when removed from allowed users. This will continue as long as they are the executor.

### Response

A successful response contains the updated wish:

```json
{
    "status": 200,
    "success": true,
    "message": "The wish has been successfully updated.",
    "data": {
        "wish": "Wish"
    }
}
```

The `wish` data attribute contains a [Wish resource](/{{route}}/{{version}}/resources/wish).

Should any error occur, the `data` attribute will be empty and a proper `message` will be returned.

> {warning.fa-exclamation-triangle} Note that the wish owner can update the `status` and any other attribute of a wish
> even when it is `in_progress` and so can they delete such wish.

<a name="take"></a>
## Take

Authorized users may take other users' wishes to fulfill. After performing this action the wish will be assigned an
"in_progress" status, and the executor will be set to the requesting user.

### Endpoint

Use the following route to take a wish:

```http
PATCH /api/wishes/{id}/take
```

> {info.fa-info-circle} Note that wish owner cannot take their own wishes to fulfill. Such action will result in an
> error.

### Data parameters

There is no payload for this route.

> {info.fa-info-circle} Though there is no payload to send to the server, it is still a PATCH route because of the fact,
> that this action modifies the data in the application.

### Response

A successful response contains the updated wish:

```json
{
    "status": 200,
    "success": true,
    "message": "The wish has been successfully taken.",
    "data": {
        "wish": "Wish"
    }
}
```

The `wish` data attribute contains a [Wish resource](/{{route}}/{{version}}/resources/wish).

Should any error occur, the `data` attribute will be empty and a proper `message` will be returned.

<a name="delete"></a>
## Delete

Authorized users may delete their wishes at any time.

### Endpoint

Use the following route to delete a wish:

```http
DELETE /api/wishes/{id}
```

### Data parameters

There is no payload for this route.

### Response

After successful deletion the following response will be returned:

```json
{
    "status": 200,
    "success": true,
    "message": "The wish has been successfully deleted.",
    "data": null
}
```

Should any error occur, a proper `message` will be returned.
