# Account

- [User details](#user-details)
- [Password change](#password-change)

<a name="user-details"></a>
## User details

Although the successful authentication response provides the user data, it sometimes might be required for your
application to access the details again. Therefore, the authorized user data can be requested at any time.

### Endpoint

| Method | URI |
|:-------|:----|
| GET   | `/api/user` |

### Data parameters

No parameters are required for this request.

### Response

A successful response has the structure presented below.

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "user": "User"
    }
}
```

The `user` attribute contains a [User resource](/{{route}}/{{version}}/resources/user).

### Examples

<br>

#### Successful request

The following is an example of a successful request and response containing user details:

```http
GET /api/user
```

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "user": {
            "id": 1,
            "name": "Administrator",
            "email": "admin@wishlist",
            "email_verified_at": null,
            "created_at": "2021-01-23T16:46:28.000000Z",
            "updated_at": "2021-01-23T16:46:28.000000Z"
        }
    }
}
```

<a name="password-change"></a>
## Password change

Authorized users can change their password to a new one at any time.

> {info.fa-info-circle} Note that users can change their current passwords to the same ones. So if you decide to change your old password
> from `1234` to `1234`, that's perfectly fine.

### Endpoint

```http
POST /api/password/change
```

### Data parameters

The following data parameters are required.

```json
{
    "current_password": "any",
    "new_password": "any",
    "new_password_confirmation": "any"
}
```

The `any` type actually refers here to `string` or `number` as no one really uses anything else, but any "password-like"
type value will be accepted here.

### Response

A successful response has the structure presented below.

```json
{
    "status": 200,
    "success": true,
    "message": "The password has been successfully changed.",
    "data": null
}
```

The cases of any error response are covered by the [Common responses](/{{route}}/{{version}}/getting-starter/common-responses)
section.

### Examples

The following is an example of successful password change:

```http
POST /api/password/change?current_password=123&new_password=123456&new_password_confirmation=123456
```

```json
{
    "status": 200,
    "success": true,
    "message": "The password has been successfully changed.",
    "data": null
}
```
