# Wishlist Server

You are browsing the Wishlist server documentation for developers.
<br>
<br>
This documentation provides full information about the server [API](https://en.wikipedia.org/wiki/API). It was also the
foundation for creating the server application during the [Behavior-Driven Development](https://en.wikipedia.org/wiki/Behavior-driven_development)
process! Therefore, if something cannot be found in this documentation, it surely does not exist in the application
either.
<br><br>
Let me [introduce you](/{{route}}/{{version}}/introduction/overview) to the application.
