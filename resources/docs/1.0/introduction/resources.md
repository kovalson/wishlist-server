# Resources

On an abstract level the Wishlist Server provides the API for managing resources and the relationships between them. To
do that, the server accepts HTTP JSON requests and returns JSON responses. Depending on the request, the response may
contain a specific resource (or more resources). As mentioned before the application consists of *users* and *wishes*.
These two are the resources.
<br>
<br>
The resources may be considered as specific data structures that repeat throughout the application, hence the
structuring was required. This structure is static for all responses, though some requests may need additional data
attributes unrelated to the resource in order to create or modify it.

> {info.fa-info-circle} The option of filtering and selecting attributes returned in responses was considered as the returned data might be
redundant. In result, the JSON response packet would get unnecessarily enlarged. The idea was abandoned, though, as such
filtering would break the consistency of responses and would automatically require the request to be enlarged by the
filters which would result in bigger packets anyway.
