# Overview

The Wishlist Server provides RESTful API for managing your wishes and fulfill those of other users. Any user can create
and maintain their own list of wishes, where they specify what they would like to get as a gift. You may consider
Wishlist as another todo app but with several enhancements.

## Base features

The Wishlist Server provides the following features:

>- making, describing and managing your wishes,
>- browsing and filtering other users wishes,
>- fulfilling the wishes of others (as well as having your wishes fulfilled!),
>- being up to date with your relatives' and friends' preferences.

## Key features

### Making a wish

You can make a wish by creating one. You only need to specify some basic information such as the name and the due date,
and that's it. You can also provide additional description or define which users (all by default) are allowed to fulfill
that wish. After the wish is created, chosen users can see it.

> {info.fa-info-circle} *(not only for developers)* <br>
Exhausting description may be very helpful, especially when the wish is
unconventional. Always try to best explain what it is, that you desire so much.

### Fulfilling a wish

You may declare to fulfill any other user's "open" wish. When you do, that user will be notified that one of their
wishes is being fulfilled. Neither that user nor any other will ever know, who is fulfilling that particular wish.
It will never be uncovered, unless you tell anyone. The wish itself, though, will appear as "in progress" and no one
else can fulfill it anymore (unless you decide to give up fulfilling it). When the time comes, and the owner of the wish
gets what they wanted, they (and only they) may change the wish status to "fulfilled". The wish is then discarded and
moved to archive. You may fulfill as many wishes as you want at the same time.

> {info.fa-info-circle} Note that the fact of fulfilling a wish is very conventional, and no one but the owner can claim
it is fulfilled, as it highly depends on the nature of the wish. If someone's wish is to get a bike, they can mark it as
fulfilled as soon as they get the bike but this rule does not have to hold for all kinds of wishes. Any inconsistencies
need to be explained individually between the users.

Before we further go into any specific details, there are several concepts that you need to get familiar with. Best
start with [what the resources are](introduction/resources.md).
