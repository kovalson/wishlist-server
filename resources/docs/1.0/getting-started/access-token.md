# Access token

The access token is required to access authorized endpoints. The token is created, assigned and returned in response to
the login request. After logging in, the returned token should be saved for further requests and passed as the `Bearer`
token in the `Authorization` header. For convenience, you may include the token to all requests, event those not
requiring it - the request will still be accepted.

## Examples

The following are examples of setting the `Authorization` header in different languages/programmes:

### cURL
```bash
curl \
  -H "Authorization: Bearer <ACCESS_TOKEN>" \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  http://localhost:8000/api/user
```

### JavaScript
```js
axios.post("/api/user", null, {
    headers: {
        "Authorization": "Bearer <ACCESS_TOKEN>",
        "Content-Type": "application/json",
        "Accept": "application/json",
    },
})
```

### HTTP
```http request
POST /api/user
Authorization: Bearer <ACCESS_TOKEN>
Content-Type: application/json
Accept: application/json
```
