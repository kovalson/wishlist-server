# Common responses

- [Unauthorized](#unauthorized)
- [Validation error](#validation-error)

There are some common responses that will be returned in specific cases.

<a name="unauthorized"></a>
## Unauthorized

This response will be returned every time the user is not authorized to perform the action. If you see this response, it
means that you either did not yet log in or pass the access token in request header.

```json
{
    "status": 401,
    "success": false,
    "message": "Unauthorized.",
    "data": null
}
```

<a name="validation-error"></a>
## Validation error

This response will be returned every time the user did not provide valid request data. The errors contained in the
response are self-explanatory, the important thing, though, is the response structure.

```json
{
    "status": 422,
    "success": false,
    "message": "The input data is invalid.",
    "data": {
        "errors": {
            "attribute": "string[]"
        }
    }
}
```

### Examples

The following is a validation error response for login request:

```json
{
    "status": 422,
    "success": false,
    "message": "The input data is invalid.",
    "data": {
        "errors": {
            "password": [
                "Password is required."
            ]
        }
    }
}
```

<br>

The following is a validation error response for changing user's password:

```json
{
    "status": 422,
    "success": false,
    "message": "The input data is invalid.",
    "data": {
        "errors": {
            "current_password": [
                "Current password is required."
            ],
            "new_password": [
                "New password is required."
            ],
            "new_password_confirmation": [
                "The new password confirmation must match the new password."
            ]
        }
    }
}
```
