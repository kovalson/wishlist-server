# Dictionary

- [Attributes' types](#attributes-types)
- [Optional attributes](#optional-attributes)
- [Special attributes](#special-attributes)

There are some words, structures, naming conventions and other things in further parts of this documentation that
require separate section to explain them. This section tries its best to deliver. In some cases, the unclear
instructions, attributes or structures are explained directly in the sections they are contained within, but consider
this chapter as a summary for most of them.

<a name="attributes-types"></a>
## Attributes' types

Often, the request or response attributes' values are not directly indicated. Instead, they are designated as `string`,
`number` and many other types. This is done entirely intentionally as some data may differ in value, whereas the type
would remain constant. Most of the types are quite natural to understand, though I tried my best to refer to the
[basic types used in TypeScript](https://www.typescriptlang.org/docs/handbook/basic-types.html). Therefore, whenever
you see `string`, `number`, `boolean[]` or any other suspicious-looking value, you may be almost certain that this is
actually a type that is provided. Of course, it is the particular value that the real request would require, and the
real response would return. **No types are required or returned at all, everything bases on real values when using the
API**.

<br>

For example, the following code shows the potential response structure for a successful login request:

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "user": "User",
        "access_token": "string"
    }
}
```

As you can see, the `status`, `success` and `message` attributes are directly defined. These are the values. But the
`user` value is actually a type - a [User resource](/{{route}}/{{version}}/resources/user) and the `access_token` is a
`string` which means that it will vary between the requests, but the type will remain. As mentioned before, this is
solely for the purpose of documenting the code, and the real response will <ins>always</ins> contain a real value there.
The following is an example for real response corresponding to the structure above:

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "user": {
            "id": 1,
            "name": "Administrator",
            "email": "admin@wishlist",
            "email_verified_at": null,
            "created_at": "2021-01-23T16:46:28.000000Z",
            "updated_at": "2021-01-23T16:46:28.000000Z"
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1..."
    }
}
```

<a name="optional-attributes"></a>
## Optional attributes

Some request data attributes are optional. These are followed by an exclamation mark `?`. This means that they may, but
do not have to be provided at all in the request data. For example:

```json
{
    "name": "string",
    "description?": "string"
}
```

the following request requires the `name` attribute to be given and to be `string`, but the `description` attribute may
not be provided at all, and if it is, it must be a `string`.

<a name="special-attributes"></a>
## Special attributes

There are special attributes that do not refer to TypeScript.

### `fieldset`

This attribute is HTTP-Request-specific. It refers to the [sparse fieldsets](https://jsonapi.org/format/#fetching-sparse-fieldsets)
of JSON API. It mostly enables to filter the resources returned by the server. See [filtering and sorting](/{{route}}/{{version}}/resources/filtering-and-sorting)
for more details.
