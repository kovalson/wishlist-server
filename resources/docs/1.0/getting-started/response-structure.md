# Response structure

- [Structure](#structure)
- [Examples](#examples)

Each response is a JSON object and has the same structure throughout the application.

<a name="structure"></a>
## Structure

To provide consistency and readability while using Wishlist API, all responses share the following structure:

```json
{
    "status": "number",
    "success": "boolean",
    "message": "string|null",
    "data": "object|null"
}
```

The attributes and their possible values are explained below.

### Attributes

#### `status`

The `status` attribute directly stores the response status as a `number` ([see](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
for reference).

<br>

#### `success`

The `success` attribute is a `boolean` indicating whether the request was successfully processed. It is usually
supported by the `message` or `data` attributes whenever it is set to `false`.

<br>

#### `message`

The `message` attribute stores a simple message explaining the request results. It is often that when this attribute
holds any string, the `data` attribute might be `null`. This may happen for errors, exceptions or simple requests that
do not expect any particular data to be returned. Mostly, when the request is handled successfully, and the results are
returned, the `message` attribute is `null`. It is mainly used to support the understanding of the response.

<br>

#### `data`

The `data` attribute stores any data that was requested by the user or that the server needs to exchange, therefore it
is almost always an `object`. Whether it is some resource or important piece of data, it will be stored in the `data`
attribute. There are responses (exceptions, errors, etc.) that may not provide any data. In this case `null` value is
returned. The actual results are keyed by the corresponding nested resources attributes (see [examples](#examples)
below).

<a name="examples"></a>
## Examples

The following is an example response for a successful login request:

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "user": {
            "id": 1,
            "name": "Administrator",
            "email": "admin@wishlist",
            "email_verified_at": null,
            "created_at": "2021-01-23T16:46:28.000000Z",
            "updated_at": "2021-01-23T16:46:28.000000Z"
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNDhlYmU5YzUyOGY3NDE5YzdhNjAwZmM4MzA2MGQxOWY4YzdhMTllZWNmMTc2OGQ5NjQ1MTU4NzhlNTJjYzg3NGRhNzllNmM2YjM0ZDU5ODgiLCJpYXQiOiIxNjExNjgzODA1Ljk1MTY2MyIsIm5iZiI6IjE2MTE2ODM4MDUuOTUxNjY4IiwiZXhwIjoiMTY0MzIxOTgwNS42NjQ4MTMiLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.KERvrMoPrdyT7Ec09lDbkzw6f7AzKmPAcmNdo6rS9_vOrVFes8n6MYgXIpZKMLEwbLwulxJvetEDq8uzBmV9HwUrJDxjn7qjHTGVAOG3ItdODdh2f0Ls4SWC_6c8VG4Ok0SYKvUpOuQCeOQSadzL6icSf-svyvWz8VxPqaWiAOP6lcyipug4GE2HqsM7FFGmyo92801aQnolPu8KiwXE3DthNYejS64_75UFMmF2TgO_iQdVechxsI00N4zfXH5lUNRd0H7-3lNXezamilp2vc_pjOt2SF8J2krKVrkQ8SBpWXeCpdTwuUlkyZ-axHtlbj10Tm1GXviEVdO0XOoPy10uB_Et4Tz-JZ07Tr12FJVA8FqoItuCbEAi4-hwh9BZJZ1afrPlfER1swXcCjRgBAbOTpCkMXLxtwILAmNZ9-tWhSyCPwDA4mmSY1_S0m4h5rp-Zno-vBZc4K0MUdtqL3T8lZy1mXbiv3Lie0dJh7PAP_uIF9Ma5sxSf6Yf-eydAyjgyQOQrkDHE4ppbDXyQcEFVJIZKbV7891QJLACuD5BKU2zMGh_BA2BQ-HCuodQTySb5bKhffZaX1ZFMriUi9wMd8t_KPe8nQrGW8zSG6SSQ4q8bPa6XFY6VIoiyhLvUCVgFDghSUIHtQ30aRc5KNnLpCHOHY-z1Sl8As-o1hU"
    }
}
```

<br>

The following is an example response for invalid login request (non-existing user):

```json
{
    "status": 401,
    "success": false,
    "message": "Authentication failed - user was not found.",
    "data": null
}
```
