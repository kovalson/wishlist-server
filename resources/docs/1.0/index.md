- ## Introduction

  - [Overview](/{{route}}/{{version}}/introduction/overview)
  - [Resources](/{{route}}/{{version}}/introduction/resources)

- ## Getting started

  - [Dictionary](/{{route}}/{{version}}/getting-started/dictionary)
  - [Response structure](/{{route}}/{{version}}/getting-started/response-structure)
  - [Common responses](/{{route}}/{{version}}/getting-started/common-responses)
  - [Access token](/{{route}}/{{version}}/getting-started/access-token)

- ## Resources

  - [User](/{{route}}/{{version}}/resources/user)
  - [Wish](/{{route}}/{{version}}/resources/wish)
  - [Filtering and sorting](/{{route}}/{{version}}/resources/filtering-and-sorting)
  - [Pagination](/{{route}}/{{version}}/resources/pagination)

- ## API
  
  - [Authentication](/{{route}}/{{version}}/api/authentication)
  - [Account](/{{route}}/{{version}}/api/account)
  - [Wish](/{{route}}/{{version}}/api/wish)
