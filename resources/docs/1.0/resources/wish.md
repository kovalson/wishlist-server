# Wish

- [Structure](#structure)
- [Conditional attributes](#conditional-attributes)
- [Statuses](#statuses)
- [Filters](#filters)
- [Example](#example)

The wish is the feature resource that holds details of others' desires.

<a name="structure"></a>
## Structure

The wish resource has the following structure:

| Attribute | Type | Description | Sortable |
|:----------|:-----|:------------|----------|
| `id` | `number` | Unique wish identifier | No
| `name` | `string` | The name that defines the wish | Yes
| `description` | `string|null` | *(optional)* Optional description that specifies the details | No
| `due_date` | `string|null` | *(optional)* Optional due date of fulfilling the wish | Yes
| `owner` | `User` | The wish owner - [User](/{{route}}/{{version}}/resources/user) resource | No
| `executor` | `User|null` | *(optional)* The executor of the wish - [User](/{{route}}/{{version}}/resources/user) resource | No
| `status` | `string` | Current status of the wish, see [statuses](#statuses) below | Yes
| `created_at` | `string|null` | *(optional)* The wish creation timestamp | No
| `updated_at` | `string|null` | *(optional)* The last update timestamp | No

The timestamps `created_at` and `updated_at` have the [ISO 8601](https://www.w3.org/TR/NOTE-datetime) datetime format
with a special UTC designator ("Z").

<a name="conditional-attributes"></a>
## Conditional attributes

There are attributes that will appear in the structure only under certain circumstances. These are the following:

| Attribute | Type | Condition | Description | Sortable |
|:----------|:-----|:----------|:------------|----------|
| `allowed_users` | `number[]|null` | The user requesting the resource must be the owner of that wish | The array of `number` ids of the users that are allowed to fulfill the wish. When `null`, all users can fulfill the wish | No

<a name="statuses"></a>
## Statuses

The wish has always only one status.

### `open`

This status is assigned automatically after creating a wish. It means that the wish has no executor yet and can be
fulfilled by anyone allowed.

### `in_progress`

This status is assigned automatically after other user decides to fulfill the wish and becomes the executor. Wishes that
are in progress are also visible but moved to the bottom of the list and cannot be taken again. The executor may change
their mind and restore the status to `open` if they don't want to fulfill that wish anymore. Whenever the wish is in
progress, the executor is defined.

### `fulfilled`

This is the only status assigned manually by the owner. Any other status can be changed to `fulfilled` at any time.
Fulfilled wish will not be visible by default, though the list of wishes can be filtered to show the fulfilled ones as
well.

<a name="filters"></a>
## Filters

The following [filters](/{{route}}/{{version}}/resources/filtering-and-sorting) may be applied to the Wish resource:

### `status`

Three values are accepted for this filter: `open`, `in_progress` and `fulfilled`. This filter scopes the wishes to the
ones with the wanted status. If more than one value is provided, wishes with any of given are returned.

<a name="example"></a>
## Example

The following is an example of JSON Wish resource:

```json
{
    "id": 1,
    "name": "New bike",
    "description": "<p>I would like to get a bike. No matter what bike.</p><p>I just want a bike.</p>",
    "due_date": null,
    "owner": {
        "id": 1,
        "name": "Administrator",
        "email": "admin@wishlist",
        "email_verified_at": null,
        "created_at": "2021-01-23T16:46:28.000000Z",
        "updated_at": "2021-01-23T16:46:28.000000Z"
    },
    "executor": null,
    "status": "open",
    "created_at": "2021-01-23T16:46:28.000000Z",
    "updated_at": "2021-01-23T16:46:28.000000Z"
}
```

