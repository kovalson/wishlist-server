# User

- [Structure](#structure)
- [Example](#example)

User is a basic resource of Wishlist.

<a name="structure"></a>
## Structure

The user resource has the following structure:

```json
{
    "id": "number",
    "name": "string",
    "email": "string",
    "email_verified_at": "string|null",
    "created_at": "string|null",
    "updated_at": "string|null"
}
```

| Attribute | Type | Description |
|-----------|------|-------------|
| `id` | `number` | Unique user identifier
| `name` | `string` | The user name
| `email` | `string` | Unique email address
| `email_verified_at` | `string|null` | *(optional)* Email verification timestamp
| `created_at` | `string|null` | *(optional)* The user creation timestamp
| `updated_at` | `string|null` | *(optional)* The last update timestamp

The timestamps `email_verified_at`, `created_at` and `updated_at` all have the [ISO 8601](https://www.w3.org/TR/NOTE-datetime)
datetime format with a special UTC designator ("Z").

<a name="example"></a>
## Example

The following is an example of JSON User resource:

```json
{
    "id": 1,
    "name": "Administrator",
    "email": "admin@wishlist",
    "email_verified_at": null,
    "created_at": "2021-01-23T16:46:28.000000Z",
    "updated_at": "2021-01-23T16:46:28.000000Z"
}
```
