# Filtering and sorting

Some of Wishlist resources can be filtered and sorted. This is done with the `filter` and `sort` parameters with
respect to the [JSON API](https://jsonapi.org/format/#fetching) guidelines. Depending on the request method, these
parameters need to be included either as request data or query parameters.

## Filters

The filters are applied using the [sparse fieldsets](https://jsonapi.org/format/#fetching-sparse-fieldsets) with the
`filter` key. The following is an example of including filters to the request:

```http request
GET /api/wishes?filter[state]=fulfilled
```

> {info.fa-info-circle} Note that the `[` and `]` brackets need to be percent-encoded as they are not safe URL
> characters.

Multiple filter values can be applied at once. In such case they need to be separated with comma:

```http request
GET /api/wishes?filter[state]=in_progress,fulfilled
```

> {info.fa-info-circle} Note that any invalid filter value will result in default filtering. If the default filtering
> is not explicitly defined, there is no filtering at all.

## Sorting

The sorting is applied using the `sort` parameter. To sort by a specific field, apply it as the parameter value:

```http request
GET /api/wishes?sort=name
```

The resource can be sorted by multiple fields at once. In such case the fields names must be separated with comma, and
the sorting precedence is preserved as given:

```http request
GET /api/wishes?sort=name,description,due_date
```

In order to sort in descending order apply a minus `-` symbol at the beginning of the field name you want to affect:

```http request
GET /api/wishes?sort=-due_date,name
```

Here, the wishes will be sorted first by `due_date` descending and then by their `name`s ascending.

> {info.fa-info-circle} Note that any invalid sort value will result in default sorting.

## Which resources can be filtered and sorted?

All resources have their filterable and sortable fields described on their corresponding pages. Bear in mind, though,
that these parameters can be used only while browsing (i.e. listing) the resources.
