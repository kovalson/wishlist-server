# Pagination

While browsing (i.e. listing) the resources, the pagination data will occur in the response message. It is not a
resource by itself, but is complementary for these. Whenever multiple items of a single resource are returned in
response message, the pagination will be appended. The structure will then be changed to the following:

```json
{
    "status": 200,
    "success": true,
    "message": null,
    "data": {
        "resources": {
            "data": "Resource[]",
            "links": {
                "first": "string",
                "last": "string",
                "prev": "string|null",
                "next": "string|null"
            },
            "meta": {
                "current_page": "number",
                "from": "number|null",
                "last_page": "number",
                "path": "string",
                "per_page": "number",
                "to": "number|null",
                "total": "number"
            }
        }
    }
}
```

> {info.fa-info-circle} Note that the `resources` attribute will be named after the real resource, so for Wish it will
> become `wishes`, for User it will become `users` and so forth. Also note that the resource name attribute is presented
> in plural as the response returns many items of the resource.
