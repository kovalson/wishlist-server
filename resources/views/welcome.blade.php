<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Krzysztof Tatarynowicz">

    <title>{{ config("app.name") }}</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style>
        html, body {
            color: #fff;
            background-color: #191E38;
        }

        * {
            font-family: "Quicksand", sans-serif;
        }
    </style>
</head>
<body>
    <div class="d-flex align-items-center vw-100 vh-100">
        <div class="container my-3 my-lg-5">
            <header class="text-center">
                <h1 class="fw-bold">{{ config("app.name") }}</h1>
                <p class="lead">@lang("Wishlist application server")</p>
                <p class="text-white-50 fw-light">v{{ config("app.version") }}</p>
            </header>
            <div class="text-center">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a class="link-info" target="_blank" href="/docs">@lang("Documentation")</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-info" target="_blank" href="https://gitlab.com/kovalson/wishlist-server">Gitlab</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-info" target="_blank" href="https://krzysztof.tatarynowicz.eu">kovalson</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
