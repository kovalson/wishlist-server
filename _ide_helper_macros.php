<?php

namespace {
    exit("This file should not be included, only analyzed be the IDE.");
}

namespace Illuminate\Contracts\Auth {

    use App\Models\User;

    interface Guard
    {
        /**
         * Returns currently authenticated user.
         *
         * @return User
         */
        public function user(): User;
    }
}

namespace Illuminate\Contracts\Routing {

    use Illuminate\Http\Response;

    class ResponseFactory
    {
        /**
         * Returns a fully structured response with message and data.
         *
         * @see \App\Mixins\ResponseMixin::full()
         * @param array|null $data
         * @param string|null $message
         * @param int|null $status
         * @return Response
         */
        public function full($data = null, $message = null, $status = 200): Response
        {
            return ResponseFactory::full($data, $message, $status);
        }

        /**
         * Returns a message-only structured response, where data is `null`.
         *
         * @see \App\Mixins\ResponseMixin::message()
         * @param string $message
         * @param int|null $status
         * @return Response
         */
        public function message(string $message, int|null $status = 200): Response
        {
            return ResponseFactory::message($message, $status);
        }

        /**
         * Returns a data-only structured response, where message is `null`.
         *
         * @see \App\Mixins\ResponseMixin::data()
         * @param array $data
         * @param int|null $status
         * @return Response
         */
        public function data(array $data, int|null $status = 200): Response
        {
            return ResponseFactory::data($data, $status);
        }
    }

}
