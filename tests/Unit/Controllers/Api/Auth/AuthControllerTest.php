<?php
declare(strict_types=1);
namespace Tests\Unit\Controllers\Api\Auth;

use App\Http\RequestKey;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    /**
     * @test
     */
    public function invalidCredentialsResultInErrorNotifyingAboutNonExistingUser(): void
    {
        // We prepare invalid credentials
        $invalidCredentials = [
            RequestKey::EMAIL => "nonexisting@email",
            RequestKey::PASSWORD => "anyPassword",
        ];

        // and try to log in with it...
        $response = $this->postJson(route("login"), $invalidCredentials);

        // and check if proper error was returned...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertJson([
                "data" => null,
                "success" => false,
                "status" => Response::HTTP_UNAUTHORIZED,
            ]);
    }

    /**
     * @test
     */
    public function existingUserCanLogIn(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();

        // we get the authentication data...
        $user = $authenticationResponse->getUser();
        $response = $authenticationResponse->getResponse();

        // and see if it succeeded and returned the correct data...
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE + ["data" => ["user", "access_token"]])
            ->assertJson([
                "data" => [
                    "user" => $user->toArray(),
                    "access_token" => $authenticationResponse->getAccessToken(),
                ],
                "success" => true,
            ]);
    }

    /**
     * @test
     */
    public function guestsGetErrorResponseWhenTryingToCheckUserDetails(): void
    {
        // We request the user details while being unauthorized...
        $response = $this->getJson(route("user"));

        // and see if we get an error response...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanCheckTheirDetails(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();

        // we get the authentication data...
        $user = $authenticationResponse->getUser();

        // and try to simulate them requesting their data...
        $response = $this
            ->withBearerToken($authenticationResponse->getAccessToken())
            ->getJson(route("user"));

        // and see if it worked and returned correct data...
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertExactJson([
                "data" => [
                    "user" => [
                        "id" => $user->id,
                        "name" => $user->name,
                        "email" => $user->email,
                        "email_verified_at" => $user->email_verified_at,
                        "created_at" => $user->created_at,
                        "updated_at" => $user->updated_at,
                    ],
                ],
                "message" => null,
                "status" => Response::HTTP_OK,
                "success" => true,
            ]);
    }

    /**
     * @test
     */
    public function authorizedLoginAttemptRefreshesTheUserAccessToken(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();

        // we get the authentication data...
        $user = $authenticationResponse->getUser();

        // we get the access token...
        $firstAccessToken = $authenticationResponse->getAccessToken();

        // now we make second login request...
        $secondResponse = $this
            ->withBearerToken($firstAccessToken)
            ->postJson(route("login"), [
                RequestKey::EMAIL => $user->email,
                RequestKey::PASSWORD => "example123",
            ]);

        // and get the new access token
        $secondResponse = json_decode($secondResponse->getContent(), true);
        $secondAccessToken = Arr::get($secondResponse, "data.access_token");

        // and we check if tokens differ
        $this->assertNotEquals($firstAccessToken, $secondAccessToken);
    }

    /**
     * @test
     */
    public function userCanChangeTheirPassword(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();

        // and try to simulate them changing their password...
        $response = $this
            ->withBearerToken($authenticationResponse->getAccessToken())
            ->postJson(route("password.change"), [
                RequestKey::CURRENT_PASSWORD => "123",
                RequestKey::NEW_PASSWORD => "123456",
                RequestKey::NEW_PASSWORD_CONFIRMATION => "123456",
            ]);

        // and check if the password was changed...
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
        $this->assertTrue(Hash::check("123", $authenticationResponse->getUser()->password));
    }

    /**
     * @test
     */
    public function guestCannotChangeTheirPassword(): void
    {
        // We request the password change...
        $response = $this->postJson(route("password.change"), [
            RequestKey::CURRENT_PASSWORD => "example123",
            RequestKey::NEW_PASSWORD => "123",
            RequestKey::NEW_PASSWORD_CONFIRMATION => "123",
        ]);

        // and see if we get an error response...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanLogOut(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();

        // and try to log them out...
        $response = $this
            ->withBearerToken($authenticationResponse->getAccessToken())
            ->postJson(route("logout"));

        // and see if we get a successful response...
        $response
            ->assertOk()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }

    /**
     * @test
     */
    public function userThatLoggedOutCannotAccessAuthorizedRoutes(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();

        // we get the access token...
        $token = $authenticationResponse->getAccessToken();

        // and then log the user out...
        $logoutResponse = $this
            ->withBearerToken($token)
            ->postJson(route("logout"));

        // and see if we successfully logged out...
        $logoutResponse
            ->assertOk()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);

        // and we reset the authorization guards...
        $this->resetAuth();

        // and then try to get user details...
        $detailsResponse = $this
            ->withBearerToken($token)
            ->getJson(route("user"));

        // and if we get an error response for getting details...
        $detailsResponse
            ->assertUnauthorized()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }
}
