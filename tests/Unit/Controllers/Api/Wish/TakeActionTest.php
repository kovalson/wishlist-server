<?php
declare(strict_types=1);
namespace Tests\Unit\Controllers\Api\Wish;

use App\Models\Wish;
use Tests\TestCase;

class TakeActionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.take", $params);
    }

    /**
     * @test
     */
    public function guestCannotTakeAWish(): void
    {
        // We create an example wish...
        $wish = $this->createExampleWish();

        // and we try to take it...
        $response = $this->patchJson($this->getRoute(["id" => $wish->id]));

        // and we assert that the request failed...
        $response
            ->assertUnauthorized()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }

    /**
     * @test
     */
    public function userCanOnlyTakeAWishOfOtherUser(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // we create other user that is an owner of the wish...
        $user = $this->createExampleUser("other@user");

        // then we create an example wish...
        $wish = $this->createExampleWish([
            Wish::COLUMN_STATUS => Wish::STATUS_OPEN,
            Wish::COLUMN_OWNER_ID => $user->id,
        ]);

        // and we try to take it...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->patchJson($this->getRoute(["id" => $wish->id]));

        // and we assert that the request was successful...
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }

    /**
     * @test
     */
    public function updatedWishIsReturnedAfterTaking(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // then we create an example wish...
        $wish = $this->createExampleWish([
            Wish::COLUMN_STATUS => Wish::STATUS_OPEN,
        ]);

        // and we try to take it...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->patchJson($this->getRoute(["id" => $wish->id]));

        // and we assert that the request was successful...
        $returnedWish = $response->getResourceData("wish");
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);

        // and we assert that the returned wish was updated...
        $this->assertEquals($wish->name, $returnedWish[Wish::COLUMN_NAME]);
        $this->assertEquals($wish->description, $returnedWish[Wish::COLUMN_DESCRIPTION]);
        $this->assertEquals($wish->due_date, $returnedWish[Wish::COLUMN_DUE_DATE]);
        $this->assertEquals(Wish::STATUS_IN_PROGRESS, $returnedWish[Wish::COLUMN_STATUS]);
    }

    /**
     * @test
     */
    public function userCanOnlyTakeOpenWish(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // we test for wishes in progress and fulfilled...
        foreach ([Wish::STATUS_IN_PROGRESS, Wish::STATUS_FULFILLED] as $forbiddenStatus) {

            // we create an example wish...
            $wish = $this->createExampleWish([
                Wish::COLUMN_STATUS => $forbiddenStatus,
            ]);

            // and we try to take it...
            $response = $this
                ->withBearerToken($authentication->getAccessToken())
                ->patchJson($this->getRoute(["id" => $wish->id]));

            // and we assert that the request has failed...
            $response
                ->assertForbidden()
                ->assertJsonStructure(self::RESPONSE_STRUCTURE);

        }

    }

    /**
     * @test
     */
    public function userMustBeAllowedToBeAbleToTakeAWish(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // then we create an example wish the user is allowed to take...
        $wishAllowed = $this->createExampleWish([
            Wish::COLUMN_STATUS => Wish::STATUS_OPEN,
        ]);

        // then we create an example wish the user is not allowed to take...
        $wishNotAllowed = $this->createExampleWish([
            Wish::COLUMN_STATUS => Wish::STATUS_OPEN,
        ]);
        $wishNotAllowed->allowedUsers()->sync([2]);

        // then we try to take these wishes...
        $responseAllowed = $this
            ->withBearerToken($authentication->getAccessToken())
            ->patchJson($this->getRoute(["id" => $wishAllowed->id]));
        $responseNotAllowed = $this
            ->withBearerToken($authentication->getAccessToken())
            ->patchJson($this->getRoute(["id" => $wishNotAllowed->id]));

        // and we assert that the former response is successful and the latter failed...
        $responseAllowed
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
        $responseNotAllowed
            ->assertForbidden()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }
}
