<?php
declare(strict_types=1);
namespace Tests\Unit\Controllers\Api\Wish;

use App\Models\Wish;
use Tests\TestCase;

class ShowActionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.show", $params);
    }

    /**
     * @test
     */
    public function guestCannotReadWishes(): void
    {
        // we create some wishes...
        Wish::factory()
            ->count(10)
            ->create();

        // we try to read all wishes one at a time...
        $wishesIds = Wish::all()
            ->pluck(Wish::COLUMN_ID)
            ->toArray();

        foreach ($wishesIds as $id) {
            $this
                ->getJson($this->getRoute(["id" => $id]))
                ->assertJsonStructure(self::RESPONSE_STRUCTURE)
                ->assertUnauthorized();
        }
    }

    /**
     * @test
     */
    public function userCanReadWishesHeIsAllowedTo(): void
    {
        // We authorize a user...
        $this->authorizeRequest();

        // we create some wishes...
        Wish::factory()
            ->count(10)
            ->create();

        // and we allow user to see all of them...
        $this->allowUserToSeeWishes();

        // we try to read all wishes one at a time...
        $wishesIds = Wish::all()
            ->pluck(Wish::COLUMN_ID)
            ->toArray();

        foreach ($wishesIds as $id) {
            $this
                ->getJson($this->getRoute(["id" => $id]))
                ->assertJsonStructure(self::RESPONSE_STRUCTURE)
                ->assertOk();
        }
    }

    /**
     * @test
     */
    public function userCannotReadWishesHeIsNotAllowedTo(): void
    {
        // We authorize a user...
        $this->authorizeRequest();

        // we create some wishes...
        Wish::factory()
            ->count(15)
            ->create()
            ->each(function (Wish $wish) {
                $wish->allowedUsers()->sync([2]);
            });

        // we try to read all wishes one at a time...
        $wishesIds = Wish::all()
            ->pluck(Wish::COLUMN_ID)
            ->toArray();

        foreach ($wishesIds as $id) {
            $this
                ->getJson($this->getRoute(["id" => $id]))
                ->assertJsonStructure(self::RESPONSE_STRUCTURE)
                ->assertForbidden();
        }
    }

    /**
     * @test
     */
    public function readingNonExistingWishResultsInProperMessageReturned(): void
    {
        // We authorize a user...
        $this->authorizeRequest();

        // and try to read a non-existing wish...
        $response = $this->getJson($this->getRoute(["id" => 42]));

        // and we assert that the response contains information
        // of non-existing wish...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertNotFound();
    }

    /**
     * @test
     */
    public function onlyAWishOwnerCanSeeAllowedUsers(): void
    {
        // We authorize a user...
        $this->authorizeRequest();

        // we create a wish...
        /** @var Wish $wish */
        $wish = Wish::factory()->createOne();

        // we get the wish using the route...
        $response = $this->getJson($this->getRoute(["id" => $wish->id]));

        // we assert that the returned wish doesn't contain allowed users...
        $this->assertArrayNotHasKey("allowed_users", $response);

        // we then make the authorized user an owner of the wish...
        $wish->update([
            Wish::COLUMN_OWNER_ID => (int) auth()->id(),
        ]);

        // we get the wish again...
        $response = $this->getJson($this->getRoute(["id" => $wish->id]));
        $responseWish = $response->getResourceData("wish");

        // and we assert that now the response wish contains allowed users...
        $this->assertArrayHasKey("allowed_users", $responseWish);
    }
}
