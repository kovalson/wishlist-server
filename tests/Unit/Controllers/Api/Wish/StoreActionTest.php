<?php
declare(strict_types=1);
namespace Tests\Unit\Controllers\Api\Wish;

use App\Http\RequestKey;
use App\Models\Wish;
use Tests\TestCase;

class StoreActionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.store");
    }

    /**
     * @test
     */
    public function guestCannotCreateAWish(): void
    {
        $this
            ->postJson(self::getRoute(), [
                RequestKey::NAME => "Example",
            ])
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanCreateAWish(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();
        $user = $authenticationResponse->getUser();
        $wish = $this->createExampleWish();

        // and try to create a valid wish...
        $response = $this
            ->withBearerToken($authenticationResponse->getAccessToken())
            ->postJson(self::getRoute(), [
                RequestKey::NAME => $wish->name,
                RequestKey::DESCRIPTION => $wish->description,
                RequestKey::DUE_DATE => $wish->due_date?->format("Y-m-d"),
            ]);

        // and check if it succeeded...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertCreated()
            ->assertResource("wish", [
                "name" => $wish->name,
                "description" => $wish->description,
                "due_date" => $wish->due_date?->format("Y-m-d"),
                "owner" => $user->toArray(),
                "allowed_users" => null, // we also check the allowed users, because this attribute will
                                         // appear only for the owner of the wish, so this is the case
            ]);
    }

    /**
     * @test
     */
    public function newlyCreatedWishResourceIsReturnedInResponse(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();

        // and prepare a wish to create...
        $wish = $this->createExampleWish();
        $wish->load(["owner", "executor"]);
        $wish->setAttribute("allowed_users", []);
        $wish->setHidden(["owner_id", "executor_id"]);

        // and we create a wish...
        $response = $this
            ->withBearerToken($authenticationResponse->getAccessToken())
            ->postJson(self::getRoute(), [
                RequestKey::NAME => $wish->name,
                RequestKey::DESCRIPTION => $wish->description,
                RequestKey::DUE_DATE => $wish->due_date?->format("Y-m-d"),
            ]);

        // and we check if the wish was returned in the response...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertCreated()
            ->assertResource("wish", $wish->toArray(), true);
    }

    /**
     * @test
     */
    public function newlyCreatedWishHasOpenStatus(): void
    {
        // We create a user and authenticate them...
        $authenticationResponse = $this->authenticateExampleUser();
        $wish = $this->createExampleWish();

        // and we create a wish...
        $response = $this
            ->withBearerToken($authenticationResponse->getAccessToken())
            ->postJson(self::getRoute(), [
                RequestKey::NAME => $wish->name,
            ]);

        // and we check if the status is set to "open"...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertCreated()
            ->assertResource("wish", [
                "status" => Wish::STATUS_OPEN,
            ]);
    }
}
