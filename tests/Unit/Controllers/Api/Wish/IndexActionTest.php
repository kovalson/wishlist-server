<?php
declare(strict_types=1);
namespace Tests\Unit\Controllers\Api\Wish;

use App\Models\Wish;
use App\Models\WishAllowedUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Tests\TestCase;

class IndexActionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.index");
    }

    /**
     * @test
     */
    public function userCanBrowseWishes(): void
    {
        // We create many example wishes to browse through...
        Wish::factory()
            ->count(10)
            ->create();

        // and we assert that user can browse them...
        $this
            ->authorizeRequest()
            ->getJson($this->getRoute())
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertOk();
    }

    /**
     * @test
     */
    public function wishesAreCorrectlyPaginated(): void
    {
        // We authorize a user...
        $authentication = $this->authenticateExampleUser();

        // we create many example wishes to browse through...
        Wish::factory()
            ->count(25)
            ->create();

        // and we allow user to see the wishes...
        Wish::all()->each(function (Wish $wish) use ($authentication) {
            $wish->allowedUsers()->sync([$authentication->getUser()->id]);
        });

        // and we query the wishes as a user...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->getJson($this->getRoute());

        // we check the wishes pagination...
        $response->assertOk();
        $wishes = $response->getResourceData("wishes");
        $wishesCount = count($wishes["data"]);
        $currentPage = $wishes["meta"]["current_page"];
        $totalPages = $wishes["meta"]["last_page"];

        // and we assert that the pagination is correct...
        $this->assertEquals(15, $wishesCount);
        $this->assertEquals(1, $currentPage);
        $this->assertEquals(2, $totalPages);
    }

    /**
     * @test
     */
    public function wishesCanBeSortedByName(): void
    {
        $this->authorizeRequest();
        $this->wishesCanBeSortedByColumns([Wish::COLUMN_NAME]);
        $this->wishesCanBeSortedByColumns(["-" . Wish::COLUMN_NAME]);
    }

    /**
     * @test
     */
    public function wishesCanBeSortedByDueDate(): void
    {
        $this->authorizeRequest();
        $this->wishesCanBeSortedByColumns([Wish::COLUMN_DUE_DATE]);
        $this->wishesCanBeSortedByColumns(["-" . Wish::COLUMN_DUE_DATE]);
    }

    /**
     * @test
     */
    public function wishesCanBeSortedByStatus(): void
    {
        $this->authorizeRequest();
        $this->wishesCanBeSortedByColumns([Wish::COLUMN_STATUS]);
        $this->wishesCanBeSortedByColumns(["-" . Wish::COLUMN_STATUS]);
    }

    /**
     * @test
     */
    public function wishesCanBeSortedByManyColumnsSimultaneously(): void
    {
        $this->authorizeRequest();
        $this->wishesCanBeSortedByColumns(["-" . Wish::COLUMN_DUE_DATE, Wish::COLUMN_NAME]);
        $this->wishesCanBeSortedByColumns([Wish::COLUMN_STATUS, Wish::COLUMN_NAME, Wish::COLUMN_DUE_DATE]);
    }

    /**
     * @test
     */
    public function wishesCanBeFilteredByStatus(): void
    {
        $this->authorizeRequest();
        $this->wishesCanBeFilteredByColumn(Wish::COLUMN_STATUS, Wish::STATUS_OPEN);
        $this->wishesCanBeFilteredByColumn(Wish::COLUMN_STATUS, Wish::STATUS_IN_PROGRESS);
        $this->wishesCanBeFilteredByColumn(Wish::COLUMN_STATUS, Wish::STATUS_FULFILLED);
    }

    /**
     * @test
     */
    public function userCanOnlyBrowseWishesHeIsAllowedTo(): void
    {
        // We get the first user or authenticate a new one...
        $authentication = $this->authenticateExampleUser();
        $user = $authentication->getUser();

        // we create a wish that the user is allowed to see...
        Wish::factory()
            ->count(15)
            ->create();

        // we allow user to see random wishes...
        Wish::query()
            ->inRandomOrder()
            ->limit(5)
            ->get()
            ->each(function (Wish $wish) use ($user) {
                $wish->allowedUsers()->sync([$user->id]);
            });

        // we query the wishes...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->getJson($this->getRoute());
        $wishes = $response->getResourceData("wishes");
        $wishesNames = Arr::pluck($wishes["data"], Wish::COLUMN_NAME);

        // we get the expected wishes...
        $expectedWishesNames = Wish::query()
            ->select(Wish::COLUMN_NAME)
            ->whereHas(Wish::RELATIONSHIP_ALLOWED_USERS, function (Builder $query) use ($user) {
                $query->where(WishAllowedUser::COLUMN_USER_ID, $user->id);
            })
            ->orWhereNull(Wish::RELATIONSHIP_ALLOWED_USERS)
            ->get()
            ->pluck(Wish::COLUMN_NAME)
            ->toArray();

        // we assert that user can see the wish...
        $this->assertEquals($expectedWishesNames, $wishesNames);
    }

    /**
     * Asserts that wishes can be sorted by given columns at once.
     *
     * @param array $columns
     * @return void
     */
    private function wishesCanBeSortedByColumns(array $columns): void
    {
        // We delete any previous wishes...
        Wish::query()->forceDelete();

        // we prepare some wishes...
        Wish::factory()
            ->count(10)
            ->create();

        // we allow user to see the wishes...
        Wish::all()->each(function (Wish $wish) {
            $wish->allowedUsers()->sync([auth()->id()]);
        });

        // we request the wishes and apply sorting...
        $response = $this->getJson($this->getRoute() . "?sort=" . join(",", $columns));

        // we get the wishes returned...
        $wishes = $response->getResourceData("wishes");
        $wishesNames = Arr::pluck($wishes["data"], Wish::COLUMN_NAME);

        // then we get the sorted wishes from database...
        $sortedWishesNames = Wish::query()->select(Wish::COLUMN_NAME);

        foreach ($columns as $column) {
            $order = "asc";

            if (Str::startsWith($column, "-")) {
                $column = Str::substr($column, 1);
                $order = "desc";
            }

            $sortedWishesNames->orderBy($column, $order);
        }

        $sortedWishesNames = $sortedWishesNames->get()
            ->pluck(Wish::COLUMN_NAME)
            ->toArray();

        // and we assert that the arrays are the same...
        $this->assertEquals($sortedWishesNames, $wishesNames);
    }

    /**
     * Asserts that wishes can be filtered by given column and value.
     *
     * @param string $column
     * @param string $value
     * @return void
     */
    private function wishesCanBeFilteredByColumn(string $column, string $value): void
    {
        // We delete any previous wishes...
        Wish::query()->forceDelete();

        // We prepare some wishes...
        Wish::factory()
            ->count(15)
            ->create();

        // we allow user to see the wishes...
        Wish::all()->each(function (Wish $wish) {
            $wish->allowedUsers()->sync([auth()->id()]);
        });

        // we request the wishes and apply the filter...
        $response = $this->getJson($this->getRoute() . "?filter[" . $column . "]=" . $value);

        // we get the wishes returned...
        $wishes = $response->getResourceData("wishes");
        $wishesIds = Arr::pluck($wishes["data"], [Wish::COLUMN_ID, $column]);

        // then we get the filtered wishes from database...
        $filteredWishesIds = Wish::query()
            ->select([Wish::COLUMN_ID, $column])
            ->where($column, $value)
            ->get()
            ->pluck([Wish::COLUMN_ID, $column])
            ->toArray();

        // and we assert that the wishes returned are the expected ones...
        $this->assertTrue($filteredWishesIds === $wishesIds);
    }
}
