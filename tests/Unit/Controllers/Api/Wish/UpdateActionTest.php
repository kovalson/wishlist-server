<?php
declare(strict_types=1);
namespace Tests\Unit\Controllers\Api\Wish;

use App\Http\RequestKey;
use App\Models\Wish;
use Tests\TestCase;

class UpdateActionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.update", $params);
    }

    /**
     * @test
     */
    public function guestCannotUpdateAWish(): void
    {
        // We create an example wish...
        $wish = $this->createExampleWish();

        // we try to update the wish...
        $response = $this->patchJson(
            $this->getRoute(["id" => $wish->id]),
            [
                RequestKey::DUE_DATE => now()->format("Y-m-d"),
            ]
        );

        // and we assert that the update failed...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanUpdateTheirWish(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // we create an example wish...
        $wish = $this->createExampleWish([
            Wish::COLUMN_OWNER_ID => $authentication->getUser()->id,
            Wish::COLUMN_STATUS => Wish::STATUS_OPEN,
        ]);

        // then we try to update the wish...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->patchJson(
                $this->getRoute(["id" => $wish->id]),
                [
                    RequestKey::DUE_DATE => now()->addDay()->format("Y-m-d"),
                ]
            );

        // and we assert that the update was successful...
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertJsonStructure(["data" => ["wish"]]);
    }

    /**
     * @test
     */
    public function updatedWishIsReturnedAfterUpdate(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // we create an example wish...
        $wish = $this->createExampleWish([
            Wish::COLUMN_OWNER_ID => $authentication->getUser()->id,
            Wish::COLUMN_STATUS => Wish::STATUS_OPEN,
        ]);

        // then we update the wish...
        $dueDate = now()->addDay()->format("Y-m-d");
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->patchJson(
                $this->getRoute(["id" => $wish->id]),
                [
                    RequestKey::NAME => "Foo-_bar123",
                    RequestKey::DESCRIPTION => "barbarfoo123",
                    RequestKey::DUE_DATE => $dueDate,
                    RequestKey::ALLOWED_USERS => null,
                    RequestKey::STATUS => Wish::STATUS_FULFILLED,
                ]
            );

        // and we get the wish data...
        $wish = $response->getResourceData("wish");

        // and we assert that the update was successful...
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertJsonStructure(["data" => ["wish"]]);

        // and we assert that the returned wish was updated...
        $this->assertEquals("Foo-_bar123", $wish[Wish::COLUMN_NAME]);
        $this->assertEquals("barbarfoo123", $wish[Wish::COLUMN_DESCRIPTION]);
        $this->assertEquals($dueDate, $wish[Wish::COLUMN_DUE_DATE]);
        $this->assertEmpty($wish[RequestKey::ALLOWED_USERS]);
        $this->assertEquals(Wish::STATUS_FULFILLED, $wish[Wish::COLUMN_STATUS]);
    }

    /**
     * @test
     */
    public function wishOwnerCannotChangeStatusToInProgress(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // we test for all statuses...
        foreach (Wish::getAllowedStatuses() as $status) {

            // we create an example wish...
            $wish = $this->createExampleWish([
                Wish::COLUMN_OWNER_ID => $authentication->getUser()->id,
                Wish::COLUMN_STATUS => $status,
            ]);

            // then we try to set the status to "in_progress"...
            $response = $this
                ->withBearerToken($authentication->getAccessToken())
                ->patchJson(
                    $this->getRoute(["id" => $wish->id]),
                    [
                        RequestKey::STATUS => Wish::STATUS_IN_PROGRESS,
                    ]
                );

            // and we assert that the update has failed...
            $response
                ->assertForbidden()
                ->assertJsonStructure(self::RESPONSE_STRUCTURE)
                ->assertDataNull();

        }
    }
}
