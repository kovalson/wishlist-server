<?php
declare(strict_types=1);
namespace Tests\Unit\Controllers\Api\Wish;

use App\Models\User;
use App\Models\Wish;
use Tests\TestCase;

class DeleteActionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.destroy", $params);
    }

    /**
     * @test
     */
    public function guestCannotDeleteAWish(): void
    {
        // We create an example wish...
        $wish = $this->createExampleWish();

        // then we try to delete it as guests...
        $response = $this->deleteJson($this->getRoute(["id" => $wish->id]));

        // and assert that the request failed...
        $response
            ->assertUnauthorized()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }

    /**
     * @test
     */
    public function userCanDeleteTheirWish(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // we create an example wish owned by the user...
        $wish = $this->createExampleWish([
            Wish::COLUMN_OWNER_ID => $authentication->getUser()->id,
        ]);

        // we try to delete it...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->deleteJson($this->getRoute(["id" => $wish->id]));

        // and we assert that the request succeeded and that the wish doesn't exist...
        $response
            ->assertSuccessful()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
        $this->assertNull(Wish::first());
    }

    /**
     * @test
     */
    public function userCannotDeleteAWishOfOtherUser(): void
    {
        // We create many users...
        $users = User::factory()
            ->count(5)
            ->create();

        // we get the first user
        /** @var User $firstUser */
        $firstUser = $users->first();

        // we create an example wish owned by some of these users...
        $wish = $this->createExampleWish([
            Wish::COLUMN_OWNER_ID => $firstUser->id,
        ]);

        // we authenticate another example user...
        $authentication = $this->authenticateExampleUser();

        // we try to delete the wish as the new user...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->deleteJson($this->getRoute(["id" => $wish->id]));

        // and we assert that the request failed...
        $response
            ->assertForbidden()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }
}
