<?php
declare(strict_types=1);
namespace Tests\Unit\Requests\Auth;

use App\Http\RequestKey;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;
use Tests\Unit\Requests\BaseRequestTestCase;

class ChangePasswordRequestTest extends BaseRequestTestCase
{
    /**
     * @test
     */
    public function lackingRequestDataCausesValidationError(): void
    {
        // and prepare the requests that lack some input data...
        $requests = [
            [
                "data" => [
                    RequestKey::NEW_PASSWORD => "123",
                    RequestKey::NEW_PASSWORD_CONFIRMATION => "123",
                ],
                "expectedErrors" => [RequestKey::CURRENT_PASSWORD],
            ],
            [
                "data" => [
                    RequestKey::CURRENT_PASSWORD => "examples123",
                    RequestKey::NEW_PASSWORD => "123",
                ],
                "expectedErrors" => [RequestKey::NEW_PASSWORD_CONFIRMATION],
            ],
            [
                "data" => [
                    RequestKey::CURRENT_PASSWORD => "example123",
                ],
                "expectedErrors" => [RequestKey::NEW_PASSWORD, RequestKey::NEW_PASSWORD_CONFIRMATION],
            ],
            [
                "data" => [
                    RequestKey::CURRENT_PASSWORD => "example123",
                    RequestKey::NEW_PASSWORD_CONFIRMATION => "123",
                ],
                "expectedErrors" => [RequestKey::NEW_PASSWORD],
            ],
            [
                "data" => [],
                "expectedErrors" => [RequestKey::CURRENT_PASSWORD, RequestKey::NEW_PASSWORD, RequestKey::NEW_PASSWORD_CONFIRMATION],
            ],
        ];

        // and assert that expected errors occurred...
        $this
            ->authorizeRequest()
            ->assertUnprocessableRequestInputData(route("password.change"), $requests);
    }

    /**
     * @test
     */
    public function newPasswordConfirmationAndNewPasswordMismatchCausesValidationError(): void
    {
        // and prepare the requests that lack some input data...
        $requests = [
            [
                "data" => [
                    RequestKey::CURRENT_PASSWORD => "example123",
                    RequestKey::NEW_PASSWORD => "123",
                    RequestKey::NEW_PASSWORD_CONFIRMATION => "123456",
                ],
                "expectedErrors" => [RequestKey::NEW_PASSWORD_CONFIRMATION],
            ],
        ];

        // and assert that expected errors occurred...
        $this
            ->authorizeRequest()
            ->assertUnprocessableRequestInputData(route("password.change"), $requests);
    }

    /**
     * @test
     */
    public function invalidCurrentPasswordCausesValidationError(): void
    {
        // and prepare the requests that lack some input data...
        $requests = [
            [
                "data" => [
                    RequestKey::CURRENT_PASSWORD => "1234",
                    RequestKey::NEW_PASSWORD => "123",
                    RequestKey::NEW_PASSWORD_CONFIRMATION => "123",
                ],
                "expectedErrors" => [RequestKey::CURRENT_PASSWORD],
            ],
        ];

        // and assert that expected errors occurred...
        $this
            ->authorizeRequest()
            ->assertUnprocessableRequestInputData(route("password.change"), $requests);
    }

    /**
     * @test
     */
    public function userCanChangeTheirPasswordToTheirCurrentPassword(): void
    {
        // We create and authorize a fake user...
        $user = User::create([
            RequestKey::NAME => "Example name",
            RequestKey::EMAIL => "example@email.com",
            RequestKey::PASSWORD => Hash::make("example123"),
        ]);
        Passport::actingAs($user);

        // and try to change the password to the currently used
        $response = $this->postJson(route("password.change"), [
            RequestKey::CURRENT_PASSWORD => "example123",
            RequestKey::NEW_PASSWORD => "example123",
            RequestKey::NEW_PASSWORD_CONFIRMATION => "example123",
        ]);

        // and check if the request was successful
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertSuccessful();
        $this->assertTrue(Hash::check("example123", $user->password));
    }
}
