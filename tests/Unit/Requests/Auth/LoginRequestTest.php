<?php
declare(strict_types=1);
namespace Tests\Unit\Requests\Auth;

use App\Http\RequestKey;
use Tests\Unit\Requests\BaseRequestTestCase;

class LoginRequestTest extends BaseRequestTestCase
{
    /**
     * @test
     */
    public function lackingRequestDataCausesValidationError(): void
    {
        // We prepare the requests that lack some input data...
        $requests = [
            [
                "data" => [RequestKey::EMAIL => "email@only"],
                "expectedErrors" => ["password"],
            ],
            [
                "data" => [RequestKey::PASSWORD => "123"],
                "expectedErrors" => ["email"],
            ],
            [
                "data" => [],
                "expectedErrors" => ["email", "password"],
            ],
        ];

        // and assert that expected errors occurred...
        $this->assertUnprocessableRequestInputData(route("login"), $requests);
    }

    /**
     * @test
     */
    public function invalidRequestDataTypeCausesValidationError(): void
    {
        // We prepare the requests that lack some input data...
        $requests = [
            [
                "data" => [RequestKey::EMAIL => "not_an_email"],
                "expectedErrors" => ["email"],
            ],
        ];

        // and assert that expected errors occurred...
        $this->assertUnprocessableRequestInputData(route("login"), $requests);
    }
}
