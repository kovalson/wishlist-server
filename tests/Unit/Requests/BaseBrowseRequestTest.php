<?php
declare(strict_types=1);
namespace Tests\Unit\Requests;

use App\Http\Requests\BaseBrowseRequest;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Validator;

class BaseBrowseRequestTest extends BaseRequestTestCase
{
    /**
     * Array of request rules.
     *
     * @var array
     */
    private $rules;

    /**
     * The app validator.
     *
     * @var Validator
     */
    private $validator;

    /**
     * Setup the test environment.
     *
     * @return void
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->rules = (new BaseBrowseRequest())->rules();
        $this->validator = $this->app["validator"];
    }

    /**
     * @test
     */
    public function allParametersAreOptional(): void
    {
        $this->assertValidRequest([]);
    }

    /**
     * Asserts that given request data is valid for the test request.
     *
     * @param array $data
     * @return void
     */
    protected function assertValidRequest(array $data): void
    {
        $validator = $this->validator->make($data, $this->rules);

        $this->assertTrue($validator->passes());
    }
}
