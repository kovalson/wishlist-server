<?php
declare(strict_types=1);
namespace Tests\Unit\Requests;

use Illuminate\Http\Response;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

abstract class BaseRequestTestCase extends TestCase
{
    /**
     * The request data array.
     *
     * @var array
     */
    private $requestData = [];

    /**
     * The array of expected errors inputs names.
     *
     * @var array
     */
    private $expectedErrors = [];

    /**
     * The buffer for request data.
     *
     * @var array
     */
    private $requestDataBuffer = [];

    /**
     * The buffer for expected errors.
     *
     * @var array
     */
    private $expectedErrorsBuffer = [];

    /**
     * Asserts that lacking specific request input data results
     * in error corresponding to that attribute.
     *
     * @param string|null $uri
     * @param array|null $data
     * @param string|null $method
     * @return void
     */
    protected function assertUnprocessableRequestInputData(
        ?string $uri = null,
        ?array $data = null,
        ?string $method = "postJson"
    ): void {
        $uri = $uri ?? $this->getRoute();
        $data = $data ?? $this->prepareRequestInputData();

        // We perform the requests and collect responses...
        $responses = collect($data)->map(function (array $request) use ($uri, $method) {
            return [
                "response" => $this->{$method}($uri, $request["data"]),
                "expectedErrors" => $request["expectedErrors"],
            ];
        });

        // and assert that expected errors occurred...
        $responses->each(function (array $request) {
            /** @var TestResponse $response */
            $response = $request["response"];
            $expectedErrors = $request["expectedErrors"];

            $response
                ->assertJsonStructure(self::RESPONSE_STRUCTURE)
                ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
                ->assertJsonValidationErrors($expectedErrors, "data.errors");
        });
    }

    /**
     * Asserts that given request input is required.
     *
     * @param string $uri
     * @param string|array $input
     * @return void
     */
    protected function assertRequestInputRequired(string $uri, string|array $input): void
    {
        if (!is_array($input)) {
            $input = [$input];
        }

        $this->assertUnprocessableRequestInputData($uri, [
            [
                "data" => [],
                "expectedErrors" => $input,
            ],
        ]);
    }

    /**
     * Asserts that given input data is invalid and will cause
     * an 422 Unprocessable Entity error.
     *
     * @param string $uri
     * @param string $input
     * @param mixed $value
     * @param string $method
     * @return void
     */
    protected function assertRequestInputDataInvalid(
        string $uri,
        string $input,
        mixed $value,
        string $method = "postJson"
    ): void {
        $this->assertUnprocessableRequestInputData(
            $uri,
            [
                [
                    "data" => [
                        $input => $value
                    ],
                    "expectedErrors" => [
                        $input
                    ],
                ],
            ],
            $method
        );
    }

    /**
     * Asserts that given input data value is valid.
     *
     * @param string $uri
     * @param string $input
     * @param mixed $value
     * @return void
     */
    protected function assertRequestInputDataValid(string $uri, string $input, mixed $value): void
    {
        $this
            ->postJson($uri, [$input => $value])
            ->assertOk()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }

    /**
     * Sets the request data for next request.
     *
     * @param string $input
     * @param mixed $value
     * @return $this
     */
    protected function setRequestData(string $input, mixed $value): static
    {
        $this->requestDataBuffer[$input] = $value;

        return $this;
    }

    /**
     * Sets the expected errors for next request.
     *
     * @param string|array $inputs
     * @return $this
     */
    protected function setExpectedError(string|array $inputs): static
    {
        if (!is_array($inputs)) {
            $inputs = [$inputs];
        }

        $this->expectedErrorsBuffer = $this->expectedErrorsBuffer + $inputs;

        return $this;
    }

    /**
     * Sets the next request data buffer.
     *
     * @return $this
     */
    protected function nextRequest(): static
    {
        if (!empty($this->requestDataBuffer) || !empty($this->expectedErrorsBuffer)) {
            $this->requestData[] = $this->requestDataBuffer;
            $this->expectedErrors[] = $this->expectedErrorsBuffer;
        }

        $this->requestDataBuffer = [];
        $this->expectedErrorsBuffer = [];

        return $this;
    }

    /**
     * Prepares the request input data with expected errors.
     *
     * @return array
     */
    private function prepareRequestInputData(): array
    {
        if (!empty($this->requestDataBuffer) || !empty($this->expectedErrorsBuffer)) {
            $this->requestData[] = $this->requestDataBuffer;
            $this->expectedErrors[] = $this->expectedErrorsBuffer;
        }

        $this->requestDataBuffer = [];
        $this->expectedErrorsBuffer = [];

        $requestsCount = count($this->requestData);
        $requestInputData = [];

        for ($i = 0; $i < $requestsCount; $i++) {
            $data = $this->requestData[$i];
            $expectedErrors = $this->expectedErrors[$i];

            $requestInputData[] = [
                "data" => $data,
                "expectedErrors" => $expectedErrors,
            ];
        }

        return $requestInputData;
    }
}
