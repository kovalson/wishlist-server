<?php
declare(strict_types=1);
namespace Tests\Unit\Requests\Wish;

use App\Http\RequestKey;
use Illuminate\Support\Str;
use Tests\Unit\Requests\BaseRequestTestCase;

class StoreRequestTest extends BaseRequestTestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.store");
    }

    /**
     * @test
     */
    public function lackingRequestDataCausesValidationError(): void
    {
        $this
            ->authorizeRequest()
            // Notice that we set no data at all
            ->setExpectedError("name")
            ->assertUnprocessableRequestInputData();
    }

    /**
     * @test
     */
    public function wishNameMustBeNonEmptyString(): void
    {
        $this
            ->authorizeRequest()
            ->setRequestData(RequestKey::NAME, "")
            ->setExpectedError("name")
            ->nextRequest()
            ->setRequestData("name", null)
            ->setExpectedError("name")
            ->assertUnprocessableRequestInputData();
    }

    /**
     * @test
     */
    public function nameCannotHaveMoreThan255Characters(): void
    {
        $this
            ->authorizeRequest()
            ->assertRequestInputDataInvalid(
                self::getRoute(),
                RequestKey::NAME,
                Str::random(256),
            );
    }

    /**
     * @test
     */
    public function descriptionIsOptional(): void
    {
        $this->testOptionalParameters();
    }

    /**
     * @test
     */
    public function descriptionMustBeAStringWhenPresent(): void
    {
        $this
            ->authorizeRequest()
            ->assertRequestInputDataInvalid(
                self::getRoute(),
                RequestKey::DESCRIPTION,
                123,
            );
    }

    /**
     * @test
     */
    public function dueDateIsOptional(): void
    {
        $this->testOptionalParameters();
    }

    /**
     * @test
     */
    public function dueDateMustBeAStringWhenPresent(): void
    {
        $this
            ->authorizeRequest()
            ->assertRequestInputDataInvalid(
                self::getRoute(),
                RequestKey::DUE_DATE,
                123,
            );
    }

    /**
     * @test
     */
    public function dueDateMustHaveValidFormatWhenPresent(): void
    {
        $this
            ->authorizeRequest()
            ->setRequestData(RequestKey::NAME, Str::random(64))
            ->setRequestData(RequestKey::DUE_DATE, "123")
            ->setExpectedError("due_date")
            ->assertUnprocessableRequestInputData();
    }

    /**
     * @test
     */
    public function allowedUsersAreOptional(): void
    {
        $this->testOptionalParameters();
    }

    /**
     * @test
     */
    public function allowedUsersCannotBeEmptyArrayWhenPresent(): void
    {
        $this
            ->authorizeRequest()
            ->setRequestData(RequestKey::NAME, Str::random(64))
            ->setRequestData(RequestKey::ALLOWED_USERS, [])
            ->setExpectedError("allowed_users")
            ->assertUnprocessableRequestInputData();
    }

    /**
     * @test
     */
    public function allowedUsersMustBeArrayOfIntegersWhenPresent(): void
    {
        $this
            ->authorizeRequest()
            ->setRequestData(RequestKey::NAME, Str::random(64))
            ->setRequestData(RequestKey::ALLOWED_USERS, ["a", "2"])
            ->setExpectedError([
                "allowed_users.0",
                "allowed_users.1",
            ])
            ->assertUnprocessableRequestInputData();
    }

    /**
     * @test
     */
    public function allowedUsersArrayMustContainExistingUsersIdsWhenPresent(): void
    {
        // We authorize an example user...
        $authenticatedResponse = $this->authenticateExampleUser();

        // and check that any other user id cannot be validated...
        $this
            ->withBearerToken($authenticatedResponse->getAccessToken())
            ->setRequestData(RequestKey::NAME, Str::random(64))
            ->setRequestData(RequestKey::ALLOWED_USERS, [2, 3, 4, 11, 123])
            ->setExpectedError([
                "allowed_users.0",
                "allowed_users.1",
                "allowed_users.2",
                "allowed_users.3",
                "allowed_users.4",
            ])
            ->assertUnprocessableRequestInputData();

        // and check that this user can be validated...
        $this
            ->withBearerToken($authenticatedResponse->getAccessToken())
            ->postJson(self::getRoute(), [
                RequestKey::NAME => Str::random(64),
                RequestKey::ALLOWED_USERS => [1],
            ])
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertCreated();
    }

    /**
     * Tests that optional parameters do not have to be present
     * in the request data.
     *
     * @return void
     */
    private function testOptionalParameters(): void
    {
        // We prepare the requests that lack some input data...
        $data = [
            RequestKey::NAME => Str::random(64),
        ];

        // and assert that expected errors occurred...
        $this
            ->authorizeRequest()
            ->postJson(self::getRoute(), $data)
            ->assertCreated()
            ->assertJsonStructure(self::RESPONSE_STRUCTURE);
    }
}
