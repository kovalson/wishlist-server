<?php
declare(strict_types=1);
namespace Tests\Unit\Requests\Wish;

use App\Http\RequestKey;
use App\Models\Wish;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\Unit\Requests\BaseRequestTestCase;

class UpdateRequestTest extends BaseRequestTestCase
{
    /**
     * @inheritDoc
     */
    protected static function getRoute(?array $params = []): string
    {
        return route("wishes.update", $params);
    }

    /**
     * @test
     */
    public function lackingRequestDataCausesValidationError(): void
    {
        // We authenticate an example user...
        $authentication = $this->authenticateExampleUser();

        // we create an example wish...
        $wish = $this->createExampleWish([
            Wish::COLUMN_OWNER_ID => $authentication->getUser()->id,
        ]);

        // and try to update it with empty name...
        $response = $this
            ->withBearerToken($authentication->getAccessToken())
            ->patchJson($this->getRoute(["id" => $wish->id]));

        // and we assert that the request failed...
        $response
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function wishNameMustBeNonEmptyStringWhenPresent(): void
    {
        $wish = $this->createExampleWish();

        $this
            ->authorizeRequest()
            ->assertRequestInputDataInvalid(
                self::getRoute(["id" => $wish->id]),
                RequestKey::NAME,
                "",
                "patchJson"
            );
    }

    /**
     * @test
     */
    public function nameCannotHaveMoreThan255Characters(): void
    {
        $wish = $this->createExampleWish();

        $this
            ->authorizeRequest()
            ->assertRequestInputDataInvalid(
                self::getRoute(["id" => $wish->id]),
                RequestKey::NAME,
                Str::random(256),
                "patchJson"
            );
    }

    /**
     * @test
     */
    public function descriptionMustBeAStringWhenPresent(): void
    {
        $wish = $this->createExampleWish();

        $this
            ->authorizeRequest()
            ->assertRequestInputDataInvalid(
                self::getRoute(["id" => $wish->id]),
                RequestKey::DESCRIPTION,
                123,
                "patchJson"
            );
    }

    /**
     * @test
     */
    public function dueDateMustBeAStringWhenPresent(): void
    {
        $wish = $this->createExampleWish();

        $this
            ->authorizeRequest()
            ->assertRequestInputDataInvalid(
                self::getRoute(["id" => $wish->id]),
                RequestKey::DUE_DATE,
                123,
                "patchJson"
            );
    }

    /**
     * @test
     */
    public function dueDateMustHaveValidFormatWhenPresent(): void
    {
        $wish = $this->createExampleWish();

        $this
            ->authorizeRequest()
            ->setRequestData(RequestKey::DUE_DATE, "123")
            ->setExpectedError("due_date")
            ->assertUnprocessableRequestInputData(
                $this->getRoute(["id" => $wish->id]),
                null,
                "patchJson"
            );
    }

    /**
     * @test
     */
    public function allowedUsersCannotBeEmptyArrayWhenPresent(): void
    {
        $wish = $this->createExampleWish();

        $this
            ->authorizeRequest()
            ->setRequestData(RequestKey::ALLOWED_USERS, [])
            ->setExpectedError("allowed_users")
            ->assertUnprocessableRequestInputData(
                $this->getRoute(["id" => $wish->id]),
                null,
                "patchJson"
            );
    }

    /**
     * @test
     */
    public function allowedUsersMustBeArrayOfIntegersWhenPresent(): void
    {
        $wish = $this->createExampleWish();

        $this
            ->authorizeRequest()
            ->setRequestData(RequestKey::ALLOWED_USERS, ["a", "2"])
            ->setExpectedError([
                "allowed_users.0",
            ])
            ->assertUnprocessableRequestInputData(
                $this->getRoute(["id" => $wish->id]),
                null,
                "patchJson"
            );
    }

    /**
     * @test
     */
    public function allowedUsersArrayMustContainExistingUsersIdsWhenPresent(): void
    {
        // We authorize an example user...
        $authenticatedResponse = $this->authenticateExampleUser();

        // we create an example wish...
        $wish = $this->createExampleWish([
            Wish::COLUMN_OWNER_ID => $authenticatedResponse->getUser()->id,
        ]);

        // and check that any other user id cannot be validated...
        $this
            ->withBearerToken($authenticatedResponse->getAccessToken())
            ->setRequestData(RequestKey::ALLOWED_USERS, [1515, 4, 5, 11, 123])
            ->setExpectedError([
                "allowed_users.0",
                "allowed_users.1",
                "allowed_users.2",
                "allowed_users.3",
                "allowed_users.4",
            ])
            ->assertUnprocessableRequestInputData(
                $this->getRoute(["id" => $wish->id]),
                null,
                "patchJson"
            );

        // and check that this user can be validated...
        $this
            ->withBearerToken($authenticatedResponse->getAccessToken())
            ->patchJson(
                $this->getRoute(["id" => $wish->id]),
                [
                    RequestKey::ALLOWED_USERS => [1],
                ]
            )
            ->assertJsonStructure(self::RESPONSE_STRUCTURE)
            ->assertSuccessful();
    }
}
