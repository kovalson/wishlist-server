<?php
declare(strict_types=1);
namespace Tests;

use App\Http\RequestKey;
use App\Models\User;
use App\Models\Wish;
use App\Repositories\Interfaces\UserRepository;
use App\Repositories\Interfaces\WishRepository;
use App\Testing\AuthenticationResponse;
use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Laravel\Passport\ClientRepository;
use ReflectionException;
use ReflectionProperty;

abstract class TestCase extends BaseTestCase
{
    /**
     * The structure of each response.
     *
     * @type array
     */
    const RESPONSE_STRUCTURE = [
        "status",
        "success",
        "message",
        "data",
    ];

    /**
     * Returns the route used in test case.
     *
     * @param array|null $params
     * @return string
     */
    protected static function getRoute(?array $params = []): string
    {
        return "";
    }

    use CreatesApplication;
    use RefreshDatabase;

    /**
     * The user repository.
     *
     * @var UserRepository
     */
    private $userRepository;

    /**
     * The wish repository.
     *
     * @var WishRepository
     */
    private $wishRepository;

    /**
     * Setup the test environment.
     *
     * @return void
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadRepositories();

        $this->createPersonalAccessClient();
    }

    /**
     * Sets the bearer token for "Authorization" header.
     *
     * @param string $token
     * @return $this
     */
    protected function withBearerToken(string $token): static
    {
        $this->defaultHeaders["Authorization"] = "Bearer " . $token;

        return $this;
    }

    /**
     * Sets the bearer token for "Authorization" header.
     *
     * @return $this
     */
    protected function withoutBearerToken(): static
    {
        if (isset($this->defaultHeaders["Authorization"])) {
            unset($this->defaultHeaders["Authorization"]);
        }

        return $this;
    }

    /**
     * Creates an example user for tests.
     *
     * @param string|null $email
     * @param string|null $password
     * @return User
     */
    protected function createExampleUser(?string $email = "example@user", ?string $password = "123"): User
    {
        $name = str_replace("@", " ", $email);

        return User::create([
            User::COLUMN_NAME => $name,
            User::COLUMN_EMAIL => $email,
            User::COLUMN_PASSWORD => Hash::make($password),
        ]);
    }

    /**
     * Creates and authorizes an example user for tests.
     *
     * @param string|null $email
     * @param string|null $password
     * @return AuthenticationResponse
     */
    protected function authenticateExampleUser(
        ?string $email = "example@user",
        ?string $password = "123"
    ): AuthenticationResponse {
        // We create a user
        $user = $this->createExampleUser($email, $password);

        // We log the user in with the login route
        $response = $this->postJson(route("login"), [
            RequestKey::EMAIL => $email,
            RequestKey::PASSWORD => $password,
        ]);

        return new AuthenticationResponse($user, $response);
    }

    /**
     * Resets the authorization guards.
     * This method comes directly from https://stackoverflow.com/questions/57813795/method-illuminate-auth-requestguardlogout-does-not-exist-laravel-passport
     * as it solves the problem of caching the authed user withing the Auth manager.
     *
     * @param array|null $guards
     * @return void
     */
    protected function resetAuth(?array $guards = null): void
    {
        $guards = $guards ?: array_keys(config("auth.guards"));

        foreach ($guards as $guard) {
            $guard = $this->app["auth"]->guard($guard);

            if ($guard instanceof SessionGuard) {
                $guard->logout();
            }
        }

        try {
            $protectedProperty = new ReflectionProperty($this->app["auth"], "guards");
            $protectedProperty->setAccessible(true);
            $protectedProperty->setValue($this->app["auth"], []);
        } catch (ReflectionException $exception) {
            echo $exception->getTraceAsString();
            exit;
        }
    }

    /**
     * Authorizes the request with fake bearer token.
     *
     * @return $this
     */
    protected function authorizeRequest(): static
    {
        $this->resetAuth();

        return $this->withBearerToken(
            $this->authenticateExampleUser()->getAccessToken()
        );
    }

    /**
     * Allows given user to see given collection of wishes.
     * If no collection is given, all wishes are considered.
     * If no user is given, the currently authorized user is considered.
     *
     * @param Collection|null $wishes
     * @param User|null $user
     * @return $this
     */
    protected function allowUserToSeeWishes(?Collection $wishes = null, ?User $user = null): static
    {
        $wishes = $wishes ?? Wish::all();
        $user = $user ?? auth()->user();

        $wishes->each(function (Wish $wish) use ($user) {
            $wish->allowedUsers()->sync([$user->id]);
        });

        return $this;
    }

    /**
     * Creates an example wish and returns it.
     *
     * @param array|null $attributes
     * @param bool|null $save
     * @return Wish
     */
    protected function createExampleWish(?array $attributes = [], ?bool $save = false): Wish
    {
        /** @var Wish $wish */
        $wish = Wish::factory()->createOne($attributes);

        if ($save) {
            $wish->save();
        }

        return $wish;
    }

    /**
     * Loads the resources repositories.
     *
     * @return void
     * @throws BindingResolutionException
     */
    private function loadRepositories(): void
    {
        $this->userRepository = $this->app->make(UserRepository::class);
        $this->wishRepository = $this->app->make(WishRepository::class);
    }

    /**
     * Creates personal access client to make authorized API calls possible.
     *
     * @return void
     */
    private function createPersonalAccessClient(): void
    {
        if (Schema::hasTable("oauth_clients")) {
            resolve(ClientRepository::class)->createPersonalAccessClient(
                null,
                config("app.name") . " Personal Access Client",
                config("app.url")
            );
        }
    }
}
