<?php

use App\Http\Controllers\DocsifyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", function () {
    return view("welcome");
});

//Route::group(["prefix" => "docs"], function () {
//    Route::get("/", [DocsifyController::class, "index"])->name("docs.index");
//    Route::get("/{path}", [DocsifyController::class, "get"])->name("docs.get")->where("path", ".*");
//});
