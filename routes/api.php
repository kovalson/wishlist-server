<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\WishController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware("json.response")->group(function () {
    Route::middleware("auth:api")->group(function () {
        // Authorized user
        Route::get("/user", [AuthController::class, "user"])->name("user");
        Route::post("/password/change", [AuthController::class, "changePassword"])->name("password.change");
        Route::post("/logout", [AuthController::class, "logout"])->name("logout");

        // Wishes
        Route::post("/wishes", [WishController::class, "store"])->name("wishes.store");
        Route::get("/wishes", [WishController::class, "index"])->name("wishes.index");
        Route::get("/wishes/{id}", [WishController::class, "show"])->name("wishes.show");
        Route::patch("/wishes/{id}", [WishController::class, "update"])->name("wishes.update");
        Route::patch("/wishes/{id}/take", [WishController::class, "take"])->name("wishes.take");
        Route::delete("/wishes/{id}", [WishController::class, "destroy"])->name("wishes.destroy");
    });

    Route::post("/login", [AuthController::class, "login"])->name("login");
});
