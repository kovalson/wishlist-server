<?php
declare(strict_types=1);
namespace App\Traits;

use App\Http\Requests\BaseBrowseRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Trait SortableTrait
 * Trait for all Eloquent Models that may be sorted using the request "sort" parameter.
 * Requires $sortableColumns array to be present in parent class to determine which
 * columns are sortable within the class. If "*" is put in the array, all columns from
 * request param are allowed as sort columns.
 *
 * @package App\Traits
 */
trait SortableTrait
{
    /**
     * Sorts the results using request "sort" parameter.
     *
     * @param Builder $query
     * @param BaseBrowseRequest $request
     * @return Builder
     */
    public function scopeSorted(Builder $query, BaseBrowseRequest $request): Builder
    {
        if (!$this->canBeSorted($request)) {
            return $query;
        }

        $requestColumns = $this->resolveRequestSortColumns($request);

        foreach ($requestColumns as $column => $order) {
            if ($this->isColumnSortable($column)) {
                $query->orderBy($column, $order);
            }
        }

        return $query;
    }

    /**
     * Checks whether the query can be sorted at all.
     *
     * @param BaseBrowseRequest $request
     * @return bool
     */
    private function canBeSorted(BaseBrowseRequest $request): bool
    {
        return (
            !is_null($request->sort) &&
            isset($this->sortableColumns) &&
            is_array($this->sortableColumns)
        );
    }

    /**
     * Resolves columns to sort from request "sort" parameter.
     *
     * @param BaseBrowseRequest $request
     * @return array
     */
    private function resolveRequestSortColumns(BaseBrowseRequest $request): array
    {
        $sortColumns = [];
        $columns = explode(",", $request->sort);

        foreach ($columns as $index => $column) {
            $isDesc = Str::startsWith($column, "-");
            $key = $isDesc ? Str::substr($column, 1) : $column;
            $order = $isDesc ? "desc" : "asc";

            $sortColumns[$key] = $order;
        }

        return $sortColumns;
    }

    /**
     * Checks whether the results may be sorted by given column.
     *
     * @param string $column
     * @return bool
     */
    private function isColumnSortable(string $column): bool
    {
        return (
            in_array($column, array_values($this->sortableColumns)) ||
            in_array("*", array_values($this->sortableColumns))
        );
    }
}
