<?php
declare(strict_types=1);
namespace App\Traits;

use App\Http\Requests\BaseBrowseRequest;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trait FilterableTrait
 * Trait for all Eloquent Models that may be filtered using the request "filter" parameter.
 * Requires $filterableColumns array to be present in parent class to determine which
 * columns are filterable within the class.
 *
 * @package App\Traits
 */
trait FilterableTrait
{
    /**
     * Filters the results using request "filter" parameter.
     *
     * @param Builder $query
     * @param BaseBrowseRequest $request
     * @return Builder
     */
    public function scopeFiltered(Builder $query, BaseBrowseRequest $request): Builder
    {
        if (!$this->canBeFiltered($request)) {
            return $query;
        }

        foreach ($request->filter as $column => $value) {
            if ($this->isColumnFilterable($column)) {
                $query->where($column, $value);
            }
        }

        return $query;
    }

    /**
     * Checks whether the query can be scoped at all.
     *
     * @param BaseBrowseRequest $request
     * @return bool
     */
    private function canBeFiltered(BaseBrowseRequest $request): bool
    {
        return (
            !is_null($request->filter) &&
            isset($this->filterableColumns) &&
            is_array($this->filterableColumns)
        );
    }

    /**
     * Checks whether the results may be filtered by given column.
     *
     * @param string $column
     * @return bool
     */
    private function isColumnFilterable(string $column): bool
    {
        return in_array($column, $this->filterableColumns);
    }
}
