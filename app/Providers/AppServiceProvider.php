<?php
declare(strict_types=1);
namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setMySQLDefaultStringLength();
    }

    /**
     * Sets the default string length for MySQL database.
     *
     * @return void
     */
    private function setMySQLDefaultStringLength(): void
    {
        $length = config("database.connections.mysql.default_string_length");

        Schema::defaultStringLength($length);
    }
}
