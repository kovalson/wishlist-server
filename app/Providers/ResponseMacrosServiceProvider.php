<?php
declare(strict_types=1);
namespace App\Providers;

use App\Mixins\ResponseMixin;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;
use ReflectionException;

class ResponseMacrosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     * @throws ReflectionException
     */
    public function boot(): void
    {
        ResponseFactory::mixin(new ResponseMixin());
    }
}
