<?php
declare(strict_types=1);
namespace App\Providers;

use App\Models\Wish;
use App\Observers\WishObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * The models' events observers to register.
     *
     * @var array
     */
    private $observers = [
        Wish::class => WishObserver::class,
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->observers as $class => $observer) {
            call_user_func([$class, "observe"], $observer);
        }
    }
}
