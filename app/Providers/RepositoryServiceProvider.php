<?php
declare(strict_types=1);
namespace App\Providers;

use App\Repositories\Eloquent\UserRepository;
use App\Repositories\Eloquent\WishRepository;
use App\Repositories\Interfaces\UserRepository as UserRepositoryInterface;
use App\Repositories\Interfaces\WishRepository as WishRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Array of services bindings.
     *
     * @var array
     */
    public $bindings = [
        UserRepositoryInterface::class => UserRepository::class,
        WishRepositoryInterface::class => WishRepository::class,
    ];
}
