<?php
declare(strict_types=1);
namespace App\Providers;

use App\Mixins\TestResponseMixin;
use Illuminate\Support\ServiceProvider;
use Illuminate\Testing\TestResponse;
use ReflectionException;

class TestResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     * @throws ReflectionException
     */
    public function boot()
    {
        TestResponse::mixin(new TestResponseMixin());
    }
}
