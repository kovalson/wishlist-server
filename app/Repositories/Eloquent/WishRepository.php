<?php
declare(strict_types=1);
namespace App\Repositories\Eloquent;

use App\Http\RequestKey;
use App\Http\Requests\Wish\BrowseRequest;
use App\Http\Resources\WishResource;
use App\Models\Wish;
use App\Repositories\Interfaces\WishRepository as WishRepositoryInterface;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WishRepository implements WishRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(array $attributes): Wish
    {
        return Wish::create($attributes);
    }

    /**
     * @inheritDoc
     */
    public function all(?BrowseRequest $request = null): ResourceCollection
    {
        $request = $request ?? request();

        $wishes = Wish::query()
            ->allowedForUser()
            ->filtered($request)
            ->sorted($request)
            ->paginate($request->per_page);

        return WishResource::collection($wishes);
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id): ?Wish
    {
        return Wish::find($id);
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $attributes): Wish
    {
        $wish = Wish::find($id);

        if (array_key_exists(RequestKey::ALLOWED_USERS, $attributes)) {
            $wish->allowedUsers()->sync($attributes[RequestKey::ALLOWED_USERS]);
            unset($attributes[RequestKey::ALLOWED_USERS]);
        }

        $wish->update($attributes);

        return $wish->refresh();
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): bool
    {
        return !!Wish::find($id)->forceDelete();
    }
}
