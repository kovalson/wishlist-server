<?php
declare(strict_types=1);
namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Interfaces\UserRepository as UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    /**
     * Update the user attributes.
     *
     * @param User|int $user
     * @param array $attributes
     * @return bool
     */
    public function update(User|int $user, array $attributes): bool
    {
        // If the ID was given, we find the user. If the user
        // does not exist, an exception is thrown
        if (is_int($user)) {
            $user = User::findOrFail($user);
        }

        // We update the user attributes
        return $user->update($attributes);
    }
}
