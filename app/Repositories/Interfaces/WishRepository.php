<?php
declare(strict_types=1);
namespace App\Repositories\Interfaces;

use App\Http\Requests\Wish\BrowseRequest;
use App\Models\Wish;
use Illuminate\Http\Resources\Json\ResourceCollection;

interface WishRepository
{
    /**
     * Creates a new wish and returns it.
     *
     * @param array $attributes
     * @return Wish
     */
    public function create(array $attributes): Wish;

    /**
     * Returns paginated wishes.
     *
     * @param BrowseRequest|null $request
     * @return ResourceCollection
     */
    public function all(?BrowseRequest $request): ResourceCollection;

    /**
     * Returns a wish by its id or null if not found.
     *
     * @param int $id
     * @return Wish|null
     */
    public function getById(int $id): ?Wish;

    /**
     * Updates the wish and returns the updated model.
     *
     * @param int $id
     * @param array $attributes
     * @return Wish
     */
    public function update(int $id, array $attributes): Wish;

    /**
     * Deletes given wish.
     * Returns "true" when succeeded.
     *
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}
