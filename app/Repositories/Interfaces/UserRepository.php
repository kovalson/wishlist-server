<?php
declare(strict_types=1);
namespace App\Repositories\Interfaces;

use App\Models\User;

interface UserRepository
{
    /**
     * Update the user attributes.
     *
     * @param User|int $user
     * @param array $attributes
     * @return bool
     */
    public function update(User|int $user, array $attributes): bool;
}
