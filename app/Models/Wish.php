<?php
declare(strict_types=1);
namespace App\Models;

use App\Http\Requests\BaseBrowseRequest;
use App\Traits\FilterableTrait;
use App\Traits\SortableTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Wish
 *
 * @method static Builder|Wish newModelQuery()
 * @method static Builder|Wish newQuery()
 * @method static Builder|Wish query()
 * @mixin Eloquent
 * @property-read Collection|User[] $allowedUsers
 * @property-read int|null $allowed_users_count
 * @property-read User $owner
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property Carbon|null $due_date
 * @property int $owner_id
 * @property int|null $executor_id
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Wish whereCreatedAt($value)
 * @method static Builder|Wish whereDescription($value)
 * @method static Builder|Wish whereDueDate($value)
 * @method static Builder|Wish whereExecutorId($value)
 * @method static Builder|Wish whereId($value)
 * @method static Builder|Wish whereName($value)
 * @method static Builder|Wish whereOwnerId($value)
 * @method static Builder|Wish whereStatus($value)
 * @method static Builder|Wish whereUpdatedAt($value)
 * @property-read User|null $executor
 * @method static Builder|Wish sorted(BaseBrowseRequest $request)
 * @method static Builder|Wish filtered(BaseBrowseRequest $request)
 * @method static Builder|Wish allowedForUser(?User $user = null)
 */
class Wish extends Model
{
    const COLUMN_ID             = "id";
    const COLUMN_NAME           = "name";
    const COLUMN_DESCRIPTION    = "description";
    const COLUMN_DUE_DATE       = "due_date";
    const COLUMN_OWNER_ID       = "owner_id";
    const COLUMN_EXECUTOR_ID    = "executor_id";
    const COLUMN_STATUS         = "status";

    const RELATIONSHIP_ALLOWED_USERS = "allowedUsers";

    const STATUS_OPEN           = "open";
    const STATUS_IN_PROGRESS    = "in_progress";
    const STATUS_FULFILLED      = "fulfilled";

    /**
     * Returns an array of all allowed statuses for a wish.
     *
     * @return array
     */
    public static function getAllowedStatuses(): array
    {
        return [
            self::STATUS_OPEN,
            self::STATUS_IN_PROGRESS,
            self::STATUS_FULFILLED,
        ];
    }

    use HasFactory;
    use FilterableTrait;
    use SortableTrait;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [
        self::COLUMN_ID,
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        self::COLUMN_DUE_DATE => "datetime",
        self::COLUMN_OWNER_ID => "integer",
        self::COLUMN_EXECUTOR_ID => "integer",
    ];

    /**
     * The columns that are sortable.
     *
     * @var string[]
     */
    protected $sortableColumns = [
        self::COLUMN_NAME,
        self::COLUMN_DUE_DATE,
        self::COLUMN_STATUS,
    ];

    /**
     * The columns that are filterable.
     *
     * @var string[]
     */
    protected $filterableColumns = [
        self::COLUMN_STATUS,
    ];

    /**
     * Returns the user it belongs to.
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, self::COLUMN_OWNER_ID);
    }

    /**
     * Returns the executor of the wish.
     *
     * @return BelongsTo
     */
    public function executor(): BelongsTo
    {
        return $this->belongsTo(User::class, self::COLUMN_EXECUTOR_ID);
    }

    /**
     * Returns the users that are allowed to fulfill the wish.
     *
     * @return BelongsToMany
     */
    public function allowedUsers(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            "wish_allowed_users",
            WishAllowedUser::COLUMN_WISH_ID,
            WishAllowedUser::COLUMN_USER_ID,
        );
    }

    /**
     * Scopes the wishes to these allowed for given user.
     * If no user is given, the currently authorized user is considered.
     *
     * @param Builder $query
     * @param User|null $user
     * @return Builder
     */
    public function scopeAllowedForUser(Builder $query, ?User $user = null): Builder
    {
        $user = $user ?? auth()->user();

        return $query
            ->whereNull(self::RELATIONSHIP_ALLOWED_USERS)
            ->orWhereHas(self::RELATIONSHIP_ALLOWED_USERS, function (Builder $query) use ($user) {
                $query->where(WishAllowedUser::COLUMN_USER_ID, $user->id);
            });
    }

    /**
     * Checks whether the wish has "open" status.
     *
     * @return bool
     */
    public function isOpen(): bool
    {
        return ($this->status === self::STATUS_OPEN);
    }

    /**
     * Checks whether the wish has "in_progress" status.
     *
     * @return bool
     */
    public function isInProgress(): bool
    {
        return ($this->status === self::STATUS_IN_PROGRESS);
    }

    /**
     * Checks whether the wish has "fulfilled" status.
     *
     * @return bool
     */
    public function isFulfilled(): bool
    {
        return ($this->status === self::STATUS_FULFILLED);
    }
}
