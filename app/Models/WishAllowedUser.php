<?php
declare(strict_types=1);
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\WishAllowedUser
 *
 * @method static Builder|WishAllowedUser newModelQuery()
 * @method static Builder|WishAllowedUser newQuery()
 * @method static Builder|WishAllowedUser query()
 * @mixin Eloquent
 * @property int $id
 * @property int $wish_id
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|WishAllowedUser whereCreatedAt($value)
 * @method static Builder|WishAllowedUser whereId($value)
 * @method static Builder|WishAllowedUser whereUpdatedAt($value)
 * @method static Builder|WishAllowedUser whereUserId($value)
 * @method static Builder|WishAllowedUser whereWishId($value)
 */
class WishAllowedUser extends Model
{
    const COLUMN_ID = "id";
    const COLUMN_WISH_ID = "wish_id";
    const COLUMN_USER_ID = "user_id";

    use HasFactory;
}
