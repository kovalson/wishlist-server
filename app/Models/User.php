<?php
declare(strict_types=1);
namespace App\Models;

use App\Repositories\Eloquent\WishRepository;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Passport\Client;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Token;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Client[] $clients
 * @property-read int|null $clients_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 */
class User extends Authenticatable
{
    const COLUMN_NAME      = "name";
    const COLUMN_EMAIL     = "email";
    const COLUMN_PASSWORD  = "password";

    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "email",
        "password",
    ];

    /**
     * The attributes that should be hidden for arrays.5
     *
     * @var array
     */
    protected $hidden = [
        "password",
        "remember_token",
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        "email_verified_at" => "datetime",
    ];

    /**
     * Checks whether the user is allowed to see given wish.
     * This method assumes that given wish exists. Otherwise, an unhandled
     * exception will be thrown.
     *
     * @param Wish|int $wish
     * @return bool
     */
    public function canSeeWish(Wish|int $wish): bool
    {
        if (is_int($wish)) {
            $wishRepository = new WishRepository();
            $wish = $wishRepository->getById($wish);
        }

        if ($this->ownsWish($wish) || $this->executesWish($wish)) {
            return true;
        }

        $isAllowedUser = $wish
            ->allowedUsers()
            ->where(WishAllowedUser::COLUMN_USER_ID, $this->id)
            ->exists();

        return ($isAllowedUser || $wish->allowedUsers->isEmpty());
    }

    /**
     * Checks whether the user owns given wish.
     * This method assumes that given wish exists. Otherwise, an unhandled
     * exception will be thrown.
     *
     * @param Wish|int $wish
     * @return bool
     */
    public function ownsWish(Wish|int $wish): bool
    {
        if (is_int($wish)) {
            $wishRepository = new WishRepository();
            $wish = $wishRepository->getById($wish);
        }

        return ($wish->owner_id === $this->id);
    }

    /**
     * Checks whether the user is an executor of given wish.
     * This method assumes that given wish exists. Otherwise, an unhandled
     * exception will be thrown.
     *
     * @param Wish|int $wish
     * @return bool
     */
    public function executesWish(Wish|int $wish): bool
    {
        if (is_int($wish)) {
            $wishRepository = new WishRepository();
            $wish = $wishRepository->getById($wish);
        }

        return ($wish->executor_id === $this->id);
    }

    /**
     * Checks whether the user can update given wish.
     * This method assumes that given wish exists. Otherwise, an unhandled
     * exception will be thrown.
     *
     * @param Wish|int $wish
     * @return bool
     */
    public function canUpdateWish(Wish|int $wish): bool
    {
        return $this->ownsWish($wish);
    }

    /**
     * Checks whether the user can delete given wish.
     * This method assumes that given wish exists. Otherwise, an unhandled
     * exception will be thrown.
     *
     * @param Wish|int $wish
     * @return bool
     */
    public function canDeleteWish(Wish|int $wish): bool
    {
        return $this->ownsWish($wish);
    }
}
