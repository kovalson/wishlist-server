<?php
declare(strict_types=1);
namespace App\Rules;

use App\Models\Wish;
use Illuminate\Contracts\Validation\Rule;

class Status implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes(mixed $attribute, mixed $value): bool
    {
        $allowedStatuses = Wish::getAllowedStatuses();

        return in_array($value, $allowedStatuses);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        $allowedStatuses = Wish::getAllowedStatuses();

        return __("The wish status is invalid. It must be one of the following: :statuses.", [
            "statuses" => join(", ", $allowedStatuses),
        ]);
    }
}
