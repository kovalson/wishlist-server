<?php
declare(strict_types=1);
namespace App\Testing;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Testing\TestResponse;

class AuthenticationResponse
{
    /**
     * The authorized user.
     *
     * @var User
     */
    private $user;

    /**
     * The authentication request response.
     *
     * @var TestResponse
     */
    private $response;

    /**
     * AuthenticationResponse constructor.
     *
     * @param User $user
     * @param TestResponse $response
     * @return void
     */
    public function __construct(User $user, TestResponse $response)
    {
        $this->user = $user;
        $this->response = $response;
    }

    /**
     * Returns the authenticated user.
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Returns the authentication response.
     *
     * @return TestResponse
     */
    public function getResponse(): TestResponse
    {
        return $this->response;
    }

    /**
     * Returns the access token or null if unauthenticated.
     *
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        if ($this->response->isOk()) {
            $content = json_decode($this->response->getContent(), true);

            return Arr::get($content, "data.access_token");
        }

        return null;
    }
}
