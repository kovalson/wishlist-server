<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\RequestKey;
use App\Http\Requests\Wish\BrowseRequest;
use App\Http\Requests\Wish\StoreRequest;
use App\Http\Requests\Wish\UpdateRequest;
use App\Http\Resources\WishResource;
use App\Models\Wish;
use App\Repositories\Interfaces\WishRepository;
use Illuminate\Http\Response;

class WishController extends Controller
{
    /**
     * The wish repository.
     *
     * @var WishRepository
     */
    private $wishRepository;

    /**
     * WishController constructor.
     *
     * @param WishRepository $wishRepository
     * @return void
     */
    public function __construct(WishRepository $wishRepository)
    {
        $this->wishRepository = $wishRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param BrowseRequest $request
     * @return Response
     */
    public function index(BrowseRequest $request): Response
    {
        $wishes = $this->wishRepository->all($request);

        return response()->data([
            "wishes" => $wishes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request): Response
    {
        $wish = $this->wishRepository->create($request->validated());

        return response()->data(
            [
                "wish" => new WishResource($wish),
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        $wish = $this->wishRepository->getById($id);

        if (is_null($wish)) {
            return $this->notFoundResponse();
        }

        if (!auth()->user()->canSeeWish($wish)) {
            return response()->message(
                __("You are not allowed to see the requested wish."),
                Response::HTTP_FORBIDDEN
            );
        }

        return response()->data([
            "wish" => new WishResource($wish),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, int $id): Response
    {
        if (count($request->validated()) === 0) {
            return response()->message(
                __("The request payload cannot be empty."),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $user = auth()->user();
        $wish = $this->wishRepository->getById($id);

        if (is_null($wish)) {
            return $this->notFoundResponse();
        }

        if (!$user->canUpdateWish($wish)) {
            return response()->message(
                __("You are not allowed to update the requested wish."),
                Response::HTTP_FORBIDDEN
            );
        }

        if ($request->input(RequestKey::STATUS) === Wish::STATUS_IN_PROGRESS) {
            return response()->message(
                __("You cannot change the status to \"in_progress\"."),
                Response::HTTP_FORBIDDEN
            );
        }

        $wish = $this->wishRepository->update($id, $request->validated());

        return response()->full(
            [
                "wish" => new WishResource($wish),
            ],
            __("The wish has been successfully updated.")
        );
    }

    /**
     * Change the wish status to "in_progress" for requesting user.
     *
     * @param int $id
     * @return Response
     */
    public function take(int $id): Response
    {
        $user = auth()->user();
        $wish = $this->wishRepository->getById($id);

        if (is_null($wish)) {
            return $this->notFoundResponse();
        }

        if ($user->ownsWish($wish)) {
            return response()->message(
                __("You cannot take your own wish to fulfill."),
                Response::HTTP_FORBIDDEN
            );
        }

        if (!$user->canSeeWish($wish)) {
            return response()->message(
                __("You are not allowed to take the requested wish."),
                Response::HTTP_FORBIDDEN
            );
        }

        if (in_array($wish->status, [Wish::STATUS_IN_PROGRESS, Wish::STATUS_FULFILLED])) {
            return response()->message(
                __("You can only take an open wish."),
                Response::HTTP_FORBIDDEN
            );
        }

        $wish = $this->wishRepository->update($id, [
            Wish::COLUMN_STATUS => Wish::STATUS_IN_PROGRESS,
            Wish::COLUMN_EXECUTOR_ID => $user->id,
        ]);

        return response()->full(
            [
                "wish" => $wish,
            ],
            __("The wish has been successfully taken.")
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        $wish = $this->wishRepository->getById($id);

        if (is_null($wish)) {
            return $this->notFoundResponse();
        }

        if (!auth()->user()->canDeleteWish($wish)) {
            return response()->message(
                __("You are not allowed to delete the requested wish."),
                Response::HTTP_FORBIDDEN
            );
        }

        $deleted = $this->wishRepository->delete($id);

        if (!$deleted) {
            return response()->message(
                __("The requested wish could not be deleted."),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return response()->message(
            __("The wish has been successfully deleted.")
        );
    }

    /**
     * Returns the default 404 Not Found response.
     *
     * @return Response
     */
    protected function notFoundResponse(): Response
    {
        return response()->message(
            __("The requested wish was not found."),
            Response::HTTP_NOT_FOUND
        );
    }
}
