<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Repositories\Interfaces\UserRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * The user repository.
     *
     * @var UserRepository
     */
    private $userRepository;

    /**
     * AuthController constructor.
     *
     * @param UserRepository $userRepository
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Handles a login request to the application.
     *
     * @param LoginRequest $request
     * @return Response
     */
    public function login(LoginRequest $request): Response
    {
        if (!auth()->attempt($request->validated())) {
            return response()->message(__("Authentication failed - user was not found."), Response::HTTP_UNAUTHORIZED);
        }

        $accessToken = auth()
            ->user()
            ->createToken("Access Token")
            ->accessToken;

        return response()->full([
            "user" => auth()->user(),
            "access_token" => $accessToken,
        ]);
    }

    /**
     * Returns authorized user data.
     *
     * @return Response
     */
    public function user(): Response
    {
        return response()->data([
            "user" => auth()->user(),
        ]);
    }

    /**
     * Changes the user password.
     *
     * @param ChangePasswordRequest $request
     * @return Response
     */
    public function changePassword(ChangePasswordRequest $request): Response
    {
        // We get the authorized user
        $user = auth()->user();

        // We update the user password
        $updated = $this->userRepository->update($user, [
            "password" => Hash::make($request->new_password),
        ]);

        if (!$updated) {
            return response()->message(
                __("The password was not updated."),
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }

        return response()->message(
            __("The password has been successfully changed."),
            Response::HTTP_OK,
        );
    }

    /**
     * Logs the user out and revokes their token.
     *
     * @return Response
     */
    public function logout(): Response
    {
        // We revoke user tokens
        $revoked = auth()
            ->user()
            ->token()
            ->revoke();

        if (!$revoked) {
            return response()->message(
                __("The user has not been logged out."),
                Response::HTTP_INTERNAL_SERVER_ERROR,
            );
        }

        return response()->message(
            __("The user has been successfully logged out."),
            Response::HTTP_OK,
        );
    }
}
