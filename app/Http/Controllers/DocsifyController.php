<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DocsifyController extends Controller
{
    /**
     * Show the documentation.
     *
     * @return View
     */
    public function index(): View
    {
        return view("docsify.docs");
    }

    /**
     * Show the documentation at given anchor.
     *
     * @param string $path
     * @return BinaryFileResponse
     */
    public function get(string $path): BinaryFileResponse
    {
        $documentationPath = base_path("docs/" . $path);

        return response()->file($documentationPath);
    }
}
