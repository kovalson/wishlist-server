<?php
declare(strict_types=1);
namespace App\Http\Requests\Wish;

use App\Http\RequestKey;
use App\Http\Requests\AbstractBaseRequest;

class StoreRequest extends AbstractBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            RequestKey::NAME                    => "required|string|max:255",
            RequestKey::DESCRIPTION             => "nullable|string",
            RequestKey::DUE_DATE                => "nullable|date_format:Y-m-d|after:today",
            RequestKey::ALLOWED_USERS           => "sometimes|nullable|array|min:1",
            RequestKey::ALLOWED_USERS . ".*"    => "integer|exists:users,id",
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            RequestKey::NAME . ".required"              => __("The wish name is required."),
            RequestKey::NAME . ".string"                => __("The wish name must be a string."),
            RequestKey::NAME . ".max"                   => __("The wish name cannot have more than :max characters."),
            RequestKey::DESCRIPTION . ".string"         => __("The wish description must be a string."),
            RequestKey::DUE_DATE . ".date_format"       => __("Invalid due date format. Valid format: YYYY-MM-DD."),
            RequestKey::DUE_DATE . ".after"             => __("Due date must be a date after today."),
            RequestKey::ALLOWED_USERS . ".array"        => __("Allowed users must be an array."),
            RequestKey::ALLOWED_USERS . ".min"          => __("Allowed users must contain at least :min user."),
            RequestKey::ALLOWED_USERS . ".*.integer"    => __("Allowed users must be an array of integers."),
            RequestKey::ALLOWED_USERS . ".*.exists"     => __("Given allowed user does not exist."),
        ];
    }
}
