<?php
declare(strict_types=1);
namespace App\Http\Requests\Auth;

use App\Http\RequestKey;
use App\Http\Requests\AbstractBaseRequest;

/**
 * Class ChangePasswordRequest
 *
 * @package App\Http\Requests\Auth
 * @property-read string $current_password
 * @property-read string $new_password
 * @property-read string $new_password_confirmation
 */
class ChangePasswordRequest extends AbstractBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            RequestKey::CURRENT_PASSWORD            => "required|password:api",
            RequestKey::NEW_PASSWORD                => "required",
            RequestKey::NEW_PASSWORD_CONFIRMATION   => "required|same:new_password",
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            RequestKey::CURRENT_PASSWORD . ".required"      => __("Current password is required."),
            RequestKey::CURRENT_PASSWORD . ".password"      => __("The current password does not match user's password."),
            RequestKey::NEW_PASSWORD . ".required"          => __("New password is required."),
            RequestKey::NEW_PASSWORD_CONFIRMATION . ".same" => __("The new password confirmation must match the new password."),
        ];
    }
}
