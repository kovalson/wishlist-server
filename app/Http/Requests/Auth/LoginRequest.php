<?php
declare(strict_types=1);
namespace App\Http\Requests\Auth;

use App\Http\RequestKey;
use App\Http\Requests\AbstractBaseRequest;

/**
 * Class LoginRequest
 *
 * @package App\Http\Requests\Auth
 * @property-read string $email
 * @property-read string $password
 */
class LoginRequest extends AbstractBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            RequestKey::EMAIL       => "required|email",
            RequestKey::PASSWORD    => "required",
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            RequestKey::EMAIL . ".required"     => __("Email address is required."),
            RequestKey::EMAIL . ".email"        => __("Given email address is not a valid email."),
            RequestKey::PASSWORD . ".required"  => __("Password is required."),
        ];
    }
}
