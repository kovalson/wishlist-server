<?php
declare(strict_types=1);
namespace App\Http\Requests;

use App\Http\RequestKey;
use App\Rules\Filter;
use App\Rules\Page;
use App\Rules\PerPage;
use App\Rules\Sort;

/**
 * Class BrowseRequest
 *
 * @package App\Http\Requests\Wish
 * @property-read int|null $page
 * @property-read int|null $per_page
 * @property-read string|null $sort
 * @property-read array|null $filter
 */
class BaseBrowseRequest extends AbstractBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            RequestKey::PAGE        => new Page(),
            RequestKey::PER_PAGE    => new PerPage(),
            RequestKey::SORT        => new Sort(),
            RequestKey::FILTER      => new Filter(),
        ];
    }
}
