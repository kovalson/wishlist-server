<?php
declare(strict_types=1);
namespace App\Http;

final class RequestKey
{
    const ALLOWED_USERS             = "allowed_users";
    const CURRENT_PASSWORD          = "current_password";
    const DESCRIPTION               = "description";
    const DUE_DATE                  = "due_date";
    const EMAIL                     = "email";
    const FILTER                    = "filter";
    const NAME                      = "name";
    const NEW_PASSWORD              = "new_password";
    const NEW_PASSWORD_CONFIRMATION = "new_password_confirmation";
    const PAGE                      = "page";
    const PASSWORD                  = "password";
    const PER_PAGE                  = "per_page";
    const SORT                      = "sort";
    const STATUS                    = "status";
}
