<?php
declare(strict_types=1);
namespace App\Http\Resources;

use App\Models\Wish;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class WishResource
 *
 * @package App\Http\Resources
 * @mixin Wish
 */
class WishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray(mixed $request): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "due_date" => $this->due_date?->format("Y-m-d"),
            "owner" => $this->owner,
            "executor" => $this->executor,
            "status" => $this->status,
            "allowed_users" => $this->when(
                $this->owner->id === auth()->id(),
                $this->allowedUsers->toArray(),
            ),
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
        ];
    }
}
