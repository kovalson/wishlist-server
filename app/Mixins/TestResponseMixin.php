<?php
declare(strict_types=1);
namespace App\Mixins;

use Closure;
use Illuminate\Testing\TestResponse;

/**
 * Class TestResponseMixin
 * Auxiliary class for TestResponse class macros during testing.
 *
 * @package App\Mixins
 * @see TestResponse
 */
class TestResponseMixin
{

    /**
     * Registers "assertResource" macro which is a shortcut for
     * assertData but with resource inside "data" attribute.
     *
     * @return Closure
     */
    public function assertResource(): Closure
    {
        return function (string $resource, ?array $array = [], ?bool $structure = false): void
        {
            /** @var TestResponse $this */
            if ($structure) {
                $this->assertJsonStructure([
                    "data" => [
                        $resource => array_keys($array),
                    ],
                ]);
            } else {
                $this->assertJson([
                    "data" => [
                        $resource => $array,
                    ],
                ]);
            }
        };
    }

    /**
     * Registers "assertDataNull" macro which asserts that the returned
     * "data" attribute contains "null" value.
     *
     * @return Closure
     */
    public function assertDataNull(): Closure
    {
        return function ()
        {
            /** @var TestResponse $this */
            $response = json_decode($this->getContent(), true);

            if (is_array($response) && isset($response["data"])) {
                $this->assertJson([
                    "data" => null,
                ]);
            }
        };
    }

    /**
     * Returns resource data from test response.
     *
     * @return Closure
     */
    public function getResourceData(): Closure
    {
        return function (string $resource): ?array
        {
            /** @var TestResponse $this */
            $response = json_decode($this->getContent(), true);

            if (is_array($response) && isset($response["data"][$resource])) {
                return $response["data"][$resource];
            }

            return null;
        };
    }
}
