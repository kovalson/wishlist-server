<?php
declare(strict_types=1);
namespace App\Mixins;

use Closure;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

/**
 * Class ResponseMixin
 * Auxiliary class for Response class macros.
 *
 * @package App\Mixins
 * @see Response
 */
class ResponseMixin
{
    /**
     * Returns a full response with data and message.
     *
     * @return Closure
     */
    public function full(): Closure
    {
        return function (?array $data = null, ?string $message = null, ?int $status = Response::HTTP_OK)
        {
            // We basically replace all resource collection data with their
            // corresponding data serialized to an associative array
            if (is_array($data)) {
                array_walk_recursive($data, function (&$value) {
                    if ($value instanceof ResourceCollection) {
                        $value = $value->response()->getData(true);
                    }
                });
            }

            return response([
                "status" => $status,
                "success" => ($status < 400),
                "message" => $message,
                "data" => $data,
            ], $status);
        };
    }

    /**
     * Returns only a message with `null` data.
     *
     * @return Closure
     */
    public function message(): Closure
    {
        return function (string $message, ?int $status = Response::HTTP_OK)
        {
            return response()->full(null, $message, $status);
        };
    }

    /**
     * Returns only data with `null` message.
     *
     * @return Closure
     */
    public function data(): Closure
    {
        return function (array $data, ?int $status = Response::HTTP_OK)
        {
            return response()->full($data, null, $status);
        };
    }
}
