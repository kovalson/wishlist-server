<?php
declare(strict_types=1);
namespace App\Observers;

use App\Models\Wish;

class WishObserver
{
    /**
     * Handle the Wish "saving" event.
     *
     * @param Wish $wish
     * @return void
     */
    public function saving(Wish $wish): void
    {
        $wish->owner_id = $wish->owner_id ?? auth()->id();
    }

    /**
     * Handle the Wish "creating" event.
     *
     * @param Wish $wish
     * @return void
     */
    public function creating(Wish $wish): void
    {
        $wish->status = $wish->status ?? "open";
    }
}
